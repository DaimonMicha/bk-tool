function kmParser() {

	this.parsePage = function() {
        this.pathList = this.splitPath(window.location.pathname);
		console.log('kmParser::parsePage ' + JSON.stringify(this.pathList));

        switch(this.pathList.shift()) {

            case 'common':
                this.parseCommon();
				break;

            case 'user':
				this.parseUser();
                break;

            case 'world':
				this.checkCooldown();
				this.parseWorld();
				break;

            case 'groupmission':
				this.checkCooldown();
				this.parseGroupmission();
                break;

            case 'tavern':
                this.parseTavern();
                break;

            case 'market':
				this.parseMarket();
                break;

            case 'treasury':
                this.parseTreasury();
                break;

            case 'duel':
				this.checkCooldown();
				this.parseDuel();
                break;

            case 'defiance':
				this.checkCooldown();
                break;

            case 'joust':
                this.parseJoust();
                break;

            case 'clan':
				this.parseClan();
                break;

            case 'clanwar':
                this.parseClanwar();
                break;

            case 'manor':
				break;

            case 'mail':
                break;

            case 'highscore':
                this.parseHighscore();
                break;

            default:
				break;

		}

	};// parsePage

    /*
     * parsing functions
     */

	this.checkCooldown = function() {
		var state = this.state.Account.cooldown || {};
		if(state.mode) delete(state.mode);
		var main = document.getElementById('mainContent');
		if(main.classList.contains("cooldownWork")) {
			state.mode = 'work';
		} else if(main.classList.contains("cooldownDuel")) {
			state.mode = 'duel';
		} else if(main.classList.contains("cooldownFight")) {
			state.mode = 'fight';
		} else if(main.classList.contains("cooldownTravel")) {
			state.mode = 'travel';
		}
		if(state.mode) {
			if(this.state.Server.progressbarEndTime !== 'undefined') {
				state.endTime = this.state.Server.progressbarEndTime;
			}
			if(this.state.Server.progressbarDuration !== 'undefined') {
				state.duration = parseInt(this.state.Server.progressbarDuration).toFixed(0);
			}
		}
		if(main.classList.contains("fightResult")) {
            if(main.classList.contains("duelFightResult")) {
                state.mode = 'duel';
            } else {
                state.mode = 'fight';
            }
            if(this.state.Server.titleTimer !== 0) {
				state.endTime = parseInt(this.state.Server.titleTimer + Date.now() / 1000).toFixed(0);
				state.duration = parseInt(this.state.Server.titleTimer).toFixed(0);
			}
		}
		this.state.Account.cooldown = state;

		console.log('checkCooldown ' + JSON.stringify(state));

		if(state.mode) return(true);
		this.state.Account.cooldown = {};
		return(false);
	};

	this.parseCommon = function() {
        switch(this.pathList.shift()) {

            case 'profile':
                var kid = parseInt(this.pathList.shift());
                if(kid === this.player) break;
                this.opponent = kid;
                var request = {action: 'bkGetKnight', account: {server: this.server, player: this.player}, knight_id: kid};
                var closure = this.bind(this.profileResponse);
                chrome.runtime.sendMessage(request, closure);
				// aktuelle Werte einlesen!
                break;

            case 'orderprofile':
				break;

            default:
                break;

        }
	};

	this.profileResponse = function(response) {
		if(chrome.runtime.lastError) {
			console.log(chrome.runtime.lastError.message);
			return;
		}
        if(response.result !== 'success') return;
		var opponent = response.knight;
		if(opponent.hitZones.length !== 5) return;

		this.ui.addPlayerZones(opponent);

		var main = document.getElementById('mainContent');
		var checker = main.querySelector('input.kmChecker');
		if(checker) {
			checker.checked = opponent.marker;
			checker.addEventListener('click', this.bind(this.knightMarker));
		}
		this.opponent = opponent;

		console.log('kmParser::profileResponse() ' + JSON.stringify(opponent));
	};

	this.knightMarker = function() {
		var main = document.getElementById('mainContent');
		var checker = main.querySelector('input.kmChecker');
		if(checker) this.opponent.marker = checker.checked;
        var request = {action: 'bkSetKnight', account: {server: this.server, player: this.player}, knight: this.opponent};
        chrome.runtime.sendMessage(request);
		console.log('knightMarker: ' + JSON.stringify(this.opponent));
	};

	this.parseUser = function() {
        var frag = this.pathList.shift();
        switch(frag) {
			case undefined:
                var request = {action: 'bkGetKnight', account: {server: this.server, player: this.player}, knight_id: this.player};
                chrome.runtime.sendMessage(request, this.bind(this.parseUserAttribute));
				break;
			case 'karma':
				this.parseUserKarma();
				break;
			case 'loot':
                var request = {action: 'bkGetKnight', account: {server: this.server, player: this.player}, knight_id: this.player};
                chrome.runtime.sendMessage(request, this.bind(this.parseUserLoot));
				break;
			default:
				console.log('parseUser: ' + frag);
				break;
		}
	};

	this.parseUserAttribute = function(response) {
		console.log('parseUserAttribute: ' + JSON.stringify(this.state.Account.player));
        this.ui.informant.setAttribute('what', 'bk_inventory');
        this.ui.informant.setAttribute('data', 1);
        this.ui.responseReady.click();
	};

	this.parseUserLoot = function(response) {
		console.log('parseUserLoot: ' + JSON.stringify(this.state.Account.player));
        this.ui.informant.setAttribute('what', 'bk_lootbox');
        this.ui.informant.removeAttribute('data');
        this.ui.responseReady.click();
	};

	this.parseUserKarma = function() {
		var state = this.state.Account.karma || {};
		state.endTime = this.state.Server.progressbarEndTime;
		state.duration = this.state.Server.progressbarDuration;
		var symbol = document.getElementById('userKarmaSymbol');
		if(symbol && symbol.classList.length > 0) {
			var skill = symbol.classList.item(symbol.classList.length - 1);
			var result = skill.split('_');
			state.modus = parseInt(result.pop());
		}
        this.state.Account.karma = state;
	};

	this.parseWorld = function() {
        switch(this.pathList.shift()) {
			case undefined:
			case 'location':
				this.parseWorldLocation();
				break;
			case 'travel':
				this.parseWorldTravel();
				break;
			default:
				break;
		}
	};

	this.parseWorldLocation = function() {
		var state = this.state.Mission || {};
		var main = document.getElementById('mainContent');
		if(main.classList.contains('location')) {
			state.location = main.classList.item(main.classList.length - 1);
			var pSpan = document.getElementById('zoneChangeCosts');
			state.points = parseInt(pSpan.innerHTML);
		} else {
			console.log('parseWorldLocation: ' + JSON.stringify(this.state.Server));
			// mission
			if(this.hasCooldown() === 'fight' && !this.state.Server.progressbarEndTime) {
				switch(state.art) {
					case 'small':
						state.points -= 20;
						break;
					case 'medium':
						state.points -= 40;
						break;
					case 'large':
						state.points -= 60;
						break;
					default:
						break;
				}
				if(state.points < 0) state.points = 0;
				this.state.Account.cooldown.mode = 'mission';
			}
		}

		this.state.Mission = state;
	};

	this.parseWorldTravel = function() {
		var main = document.getElementById('mainContent');
		if(!main.classList.contains('map')) return;

		var state = this.state.Mission || {};
		var markers = main.querySelectorAll('.marker');
		for(var i = 0; i < markers.length; ++i) {
			if(markers[i].classList.contains('origin')) {
				state.location = markers[i].parentNode.id;
				break;
			}
		}
		this.state.Mission = state;
	};

	this.parseGroupmission = function() {
        switch(this.pathList.shift()) {
			case undefined:
			case 'group':
				this.parseGroupmissionGroup();
				break;
			default:
				break;
		}
	};

	this.parseGroupmissionGroup = function() {
		if(this.hasCooldown()) {
			// we have an cooldown
			if(this.state.GM.mode === 'in_group') {
				this.state.GM.mode = 'no_points';
			}
			return;
		}

		var state = this.state.GM;
		var main = document.getElementById('mainContent');
		var soll = parseInt(state.points || 0);
		var ist = 0;
		var step = 5;
		var max = 120;

		if(this.state.Server.inGroup || this.state.Server.grpData) {
			state.mode = 'in_group';
			ist = max;
		} else {
			var content = main.querySelector('div.innerContent');
			if(content) {
				var p = content.querySelector('em');
				if(p) ist = parseInt(p.innerHTML);
			} else {
				if(main.classList.contains('groupSearch')) {
					ist = max;
					state.mode = 'search_group';
					state.groups = 0;
					// any groups?
					var groupRows = main.querySelectorAll('table#highscoreClanTable tbody tr');
					for(var i = 0; i < groupRows.length; ++i) {
						if(groupRows[i].className !== 'noBreakRow') state.groups++;
					}

					//if(groupRows) state.groups = groupRows.length;
				}
			}
		}

		if(ist > soll) {
			// vor der GM
			// adjust the timer?
		}
		if(ist < max && ist < soll) {
			// nach der GM
		}
		state.points = ist;
		if(ist < max) {
			state.mode = 'no_points';
			var diff = parseInt((max - ist) / step);
			state.endTime = parseInt(Date.now() / 1000) + parseInt(diff * 60 * 60);
		}
		state.duration = (24 * 60 * 60);

		this.state.GM = state;
		if(state.mode === 'in_group') {
			this.state.currentAction = 'GM';
		} else if(this.state.currentAction === 'GM'){
			delete(this.state.currentAction);
		}
		if(state.mode === 'search_group' && state.groups > 0) {
			this.state.currentAction = 'GM';
		}
		console.log('parseGroupmissionGroup: ' + JSON.stringify(state) + '\n' + JSON.stringify(this.state.Server));
	};

    this.parseTavern = function() {
		var state = this.state.OS || {};
        switch(this.pathList.shift()) {
            case 'prepare':
                if(this.state.Server.progressbarEndTime < 1) break;
                var main = document.getElementById('mainContent');
                if(!main.classList.contains("prepare")) break;
                if(!this.state.Server.progressbarEndTime) break;
                state.round = 0;// Kriegsvorbereitung
                state.mode = 'prepare';
                state.endTime = this.state.Server.progressbarEndTime;
                state.duration = this.state.Server.progressbarDuration;
                state.mercenary = true;
                break;
            case 'battle':
                if(this.state.Server.progressbarEndTime < 1) break;
                var main = document.getElementById('mainContent');
                if(!main.classList.contains("battle")) break;
                var battlerounds = main.querySelector("div.battlerounds");
                if(!battlerounds) break;
                var round = battlerounds.innerHTML;
                state.round = parseInt(round.split(" ")[1]);
                state.mode = 'battle';
                state.endTime = this.state.Server.progressbarEndTime;
                state.duration = this.state.Server.progressbarDuration;
                state.mercenary = true;
                break;
            case 'fight':
				break;
            case 'fightresult':
                this.opponent = this.splitSearch(this.state.queryString, 'enemyID');
                this.callReports();
                break;
            default:
                break;
        }
        this.state.OS = state;
    };

    this.parseMarket = function() {
        switch(this.pathList.shift()) {
			case 'work':
				this.parseMarketWork();
				break;
			case 'merchant':
				this.parseMarketMerchant();
				break;
			default:
				break;
		}
	};

	this.parseMarketWork = function() {
		var state = this.state.Work;
		if(this.checkCooldown()) {
			if(this.state.Account.cooldown.mode === 'work') {
				this.state.Work.mode = 'working';
				delete(this.state.Account.cooldown.data);
				return;
			}
		}
		var button;
		button = document.getElementById('encashLink');
		if(button) { // Lohn abholen
			state.mode = 'encash';
			this.state.currentAction = 'Work';
		} else {
			switch(state.mode) {
				case 'encash':
					state.mode = 'finish';
					//this.saveState();
					//state.mode = '';
					break;
/*
				case 'working':
					state.mode = 'encash';
					break;
*/
				default:
					state.mode = 'go_work';
					break;
			}
		}
		this.state.Work = state;
		console.log('parseMarketWork: ' + JSON.stringify(state));
	};

	this.parseMarketMerchant = function() {
        switch(this.pathList.shift()) {
			case 'artefacts':
				this.parseArtefacts();
				break;
			default:
				break;
		}
	};

	this.parseArtefacts = function() {
		var state = this.state.Market || {};
		var ids = [];
		var things = document.querySelectorAll('#merchItemLayer .merchantItem');
		things.forEach(function(current, index, listObj) {
			if(current.classList.contains('itemClue01_closed')) {
				//this.buyItem(current.id);
				ids.push(current.id);
			}
		}, this);
		state.buyItems = ids;
		this.state.Market = state;
		//console.log('-> parseArtefacts: ' + JSON.stringify(state));
	};

    this.parseTreasury = function() {
        if(this.state.Server.progressbarEndTime > 0) {
            this.state.Treasury.endTime = this.state.Server.progressbarEndTime;
        }
    };

	this.parseDuel = function() {
        switch(this.pathList.shift()) {
			case 'duel':
				this.parseDuelDuel();
				break;
			default:
				break;
		}
	};

	this.parseDuelDuel = function() {
		var state = this.state.Account.cooldown || {};
		if(state.mode !== 'duel') return;

		if(isNaN(state.endTime)) state.endTime = parseInt(this.state.Server.titleTimer + Date.now() / 1000).toFixed(0);
		if(isNaN(state.duration)) state.duration = parseInt(this.state.Server.titleTimer).toFixed(0);

		this.state.Account.cooldown = state;
		//this.callReports(true);
		console.log('kmParser::parseDuelDuel() ' + JSON.stringify(this.state));
	};

    this.parseJoust = function() {
        var state = this.state.Tournament || {};
        var frag = this.pathList.shift();
        switch(frag) {
            case undefined:
                var button = document.getElementById('btnApply');
                if(button) { // Vorbereitung

					var aDiv = document.getElementById('joustApply');

					if(button.classList.contains('disabledBtn')) {
						state.round = 0;
					} else {
						state.round = -1;
						//anmelden?
					}

					console.log(button.classList + ', ' + JSON.stringify(state));

				} else { // runde finden
                    state.round = this.findJoustRound();
                    state.endTime = this.state.Server.progressbarEndTime;
                    state.duration = this.state.Server.progressbarDuration;
                    break;
                }
                break;
            default:
                break;
        }
        this.state.Tournament = state;
    };

    this.findJoustRound = function() {
        var main = document.querySelector('div.paperContainerInner');
        rounds = main.getElementsByTagName('div');
        for(var i = 0; i < rounds.length; ++i) {
            var head = rounds[i];
            if(head.className === 'boardHead') {
                var text = head.querySelector('em.light').innerHTML;
                if(text.indexOf(' - ') < 0) {
                    var h4 = head.querySelector('h4');
                    return(parseInt(h4.innerHTML.split(' ')[1]));
                }
            }
        }
        return(0);
    };

    this.parseClan = function() {
		var token = this.pathList.shift() || 'leer';
        switch(token) {
			case 'leer':
				this.parseClanClan();
				break;
			case 'upgrades':
				this.parseClanUpgrades();
				break;
			default:
				break;
		}
	};

	this.parseClanClan = function() {
		var state = this.state.OK || {};
		var outer = document.querySelector('#clanFactsWrapper .iconSilver').parentElement;
		var silver = outer.querySelector('em');
		if(silver) state.current = parseInt(silver.innerHTML).toFixed(0);

		// Orden ohne Burg, oh oh der link is ja nur hier sichtbar...
		var orderUpgrades = document.getElementById('navOrderUpgrades');
		if(!orderUpgrades) {
			state.castle = false;
		} else {
			state.castle = true;
		}
		state.payment = state.payment || 0;
		this.state.OK = state;

		console.log('parseClanClan sagt ' + JSON.stringify(state));
	};

    this.parseClanUpgrades = function() {
		var state = this.state.OK || {};
		var field = document.getElementById('dev_silver');
		if(field) state.current = parseInt(field.innerHTML);
		field = document.getElementById('dev_payments');
		if(field) state.payment = parseInt(field.innerHTML);
		this.state.OK = state;
	};

    /*
     * mode: undefined, peace, prepare, battle, fight, fightresult
     */
    this.parseClanwar = function() {
		var state = this.state.OS || {};
        var main = document.getElementById('mainContent');
        switch(this.pathList.shift()) {

			case undefined:
                if(!main.classList.contains("map")) break;
                if(main.querySelector('div.scrollLongTall')) break;

                state.round = -1;// kein Krieg
                state.mode = 'peace';
                if(state.endTime) delete(state.endTime);
				break;

			// Kriegsvorbereitung
            case 'prepare':
                if(!main.classList.contains("prepare")) break;
                if(this.state.Server.progressbarEndTime < 1) break;

                state.round = 0;
                state.mode = 'prepare';
                state.endTime = this.state.Server.progressbarEndTime;
                state.duration = this.state.Server.progressbarDuration;
                state.mercenary = false;
                break;

            case 'battle':
                if(this.state.Server.progressbarEndTime < 1) break;
                if(!main.classList.contains("battle")) break;
                var battlerounds = main.querySelector("div.battlerounds");
                if(!battlerounds) break;

                var round = battlerounds.innerHTML;
                state.round = parseInt(round.split(" ")[1]);
                state.mode = 'battle';
                state.endTime = this.state.Server.progressbarEndTime;
                state.duration = parseInt(this.state.Server.progressbarDuration);
                state.mercenary = false;
                break;

            case 'fight':
				var fightKnob = main.querySelector('a.button');
				if(!fightKnob) break;

				console.log('parseClanwar(fight): ' + fightKnob.href);
                state.mode = 'fight';
				break;

            case 'fightresult':
				this.state.Account.cooldown.mode = 'duel';
                state.mode = 'fightresult';
                state.opponent = this.splitSearch(this.state.queryString, 'enemyID');
                this.callReports(true);
                break;
            default:
                break;
        }
        this.state.OS = state;
    };

    this.parseHighscore = function() {
        switch(this.pathList.shift()) {
			case 'order':
				break;
			case undefined:
				this.parseKnightsHighscore();
				break;
			default:
				break;
		}
    };

    this.parseKnightsHighscore = function() {
		var table = document.getElementById('highscoreTable');
		if(!table) return;
		var lastKnight;
		var rows = table.querySelectorAll('tbody tr');
		for(var i = 0; i < rows.length; ++i) {
			if(!rows[i].classList.contains('userSeperator')) {
				lastKnight = this.parseHighscoreRow(rows[i]);
                var request = {
					action: 'bkGetKnight',
					account: {server: this.server, player: this.player},
					knight_id: lastKnight.id,
					data: lastKnight,
					row: i
				};
                //var closure = this.bind(this.checkHighscoreKnight);
                chrome.runtime.sendMessage(request, this.bind(this.checkHighscoreKnight));
			}
		}
	};

    this.parseHighscoreRow = function(row) {
		var colKnight = {};
		var cols = row.querySelectorAll('td');
		for(var i = 0; i < cols.length; ++i) {
			if(cols[i].classList.contains('highscore02')) {
			} else if(cols[i].classList.contains('highscore03')) {
				var links = cols[i].getElementsByTagName('a');
				for(var li = 0; li < links.length; ++li) {
					var link = links[li].getAttribute('href');
					if(link && link.indexOf('/profile/') > -1) {
						colKnight.id = parseInt(link.split('/')[5]);
						var name_string = links[li].innerHTML;
						var pos = name_string.indexOf(' ');
						colKnight.rang = name_string.substr(0, pos);
						colKnight.name = name_string.substr(pos + 1);
					} else if(link && link.indexOf('/orderprofile/') > -1) {
						colKnight.cid = parseInt(link.split('/')[5]);
					}
				}
			} else if(cols[i].classList.contains('highscore04')) {
				colKnight.level = parseInt(cols[i].innerHTML);
			} else if(cols[i].classList.contains('highscore05')) {
				colKnight.loot_won = parseInt(cols[i].innerHTML.replace(/\./g,''));
			} else if(cols[i].classList.contains('highscore06')) {
				colKnight.fights = parseInt(cols[i].innerHTML.replace(/\./g,''));
			} else if(cols[i].classList.contains('highscore07')) {
				colKnight.fights_won = parseInt(cols[i].innerHTML.replace(/\./g,''));
			} else if(cols[i].classList.contains('highscore08')) {
				colKnight.fights_lose = parseInt(cols[i].innerHTML.replace(/\./g,''));
			}
		}
		return(colKnight);
	};

	this.checkHighscoreKnight = function(response) {
		var table = document.getElementById('highscoreTable');
		var rows = table.querySelectorAll('tbody tr');
		var knight = response.knight;
		var current = response.request.data;

		if(!knight.loot_won) knight.loot_won = 0;
		if(parseInt(current.loot_won) > parseInt(knight.loot_won)) {
			rows[response.request.row].style.borderLeft = '3px solid #00FF00';
		}
		knight = Object.assign(knight, current);
        var request = {
			action: 'bkSetKnight',
			account: {server: this.server, player: this.player},
			knight: knight
		};
        chrome.runtime.sendMessage(request);
	};

}
