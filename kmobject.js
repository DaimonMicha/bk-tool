class kmObject {
	bind(method) {
        var _this = this;
        return(function() {
            return(method.apply(_this, arguments));
        });
	}
}
