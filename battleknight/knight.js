class kmKnight extends kmObject {

	constructor({
		id = 0,
		cid = 0,
		name = "",
		rang = "Bürger",
		level = 0,
		silver = 0,
		rubies = 0,
		karma = 0,
		experience = 0,
		hitZones = [],
		defendZones = [],
		strength = 0,
		dexterity = 0,
		endurance = 0,
		luck = 0,
		weapon = 0,
		shield = 0,
		damageMin = 0,
		damageMax = 0,
		armour = 0.0,
		bonusSpeed = 0,
		bonusCriticalDamage = 0,
		premium = {},
		elementalDamage = {},
		elementalResistance = {},
		loot_won = 0,
		loot_lose = 0,
		fights = 0,
		fights_won = 0,
		fights_lose = 0,
		tournaments_won = 0,
		marker = 0,
		last_duel = 0,
		last_update = 0
    } = {}) {
		super();
		Object.assign(this, {
			id,
			cid,
			name,
			rang,
			level,
			silver,
			rubies,
			karma,
			hitZones,
			defendZones,
			strength,
			dexterity,
			endurance,
			luck,
			weapon,
			shield,
			damageMin,
			damageMax,
			armour,
			bonusSpeed,
			bonusCriticalDamage,
			premium,
			elementalDamage,
			elementalResistance,
			loot_won,
			loot_lose,
			fights,
			fights_won,
			fights_lose,
			tournaments_won,
			marker,
			last_duel,
			last_update
		});
	}

	merge(obj) {
		var nk = new kmKnight();
		//var props = [];
		for(var prop in nk) {
			if(obj.hasOwnProperty(prop)) {
				this[prop] = obj[prop];
			}
		}
	}

	get data() {
		var ret = {};
		Object.assign(ret, this);
		return(ret);
	}

}
