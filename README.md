# BK-Tool

An Chromium-Plugin for my daily routine in battleknight


Lade Dir die Zip-Datei herunter:
https://gitlab.com/DaimonMicha/bk-tool/-/archive/master/bk-tool-master.zip

Entpacke sie in ein Verzeichnis Deiner Wahl.

Öffne den Erweiterungen-Tab in Deinem Chromium-Browser. Stelle dort
den Entwicklermodus auf 'an'. Nun kannst Du mit dem Knopf
'Entpackte Erweiterung laden' dem Browser das Verzeichnis zeigen, in
das Du das Plugin entpackt hast.

Auf der Battleknight-Seite kannst Du jetzt über die gekreuzten Schwerter
(das Icon <img src="https://gitlab.com/DaimonMicha/bk-tool/blob/master/attack.png">) in der Tool-Leiste die einzelnen Module des Plugins
auswählen.
Die meisten Dinge sind dem Plugin am Anfang noch unbekannt. Je mehr Du
aber klickst, umso mehr Informationen sammelt das Plugin. Z.B. liest es
nach jedem Duell automatisch die Kampfberichte und wertet auch die
Trefferzonen Deiner Gegner aus. Diese siehst Du dann auf der Profil-
Seite Deines Gegners.

Jedesmal, wenn Du die Highscore-Tabelle aufrufst, markiert das Plugin
die Spieler, die seit dem letzten Aufruf Silber gewonnen haben.

Arbeiten: wenn Du das Häkchen neben Arbeit gesetzt hast, brauchst Du
Deinen Ritter nur einmal arbeiten schicken, danach geht er automatisch
weiter arbeiten.

Viel Spass & Erfolg!
