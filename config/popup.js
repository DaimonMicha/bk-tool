class optionsPopup {

	constructor() {
		this.options = new bkOptions();

        var closure = this.bind(this.listener);
        chrome.runtime.onMessage.addListener(closure);

        closure = this.bind(this.storeOptions);
        document.getElementById('saveButton').addEventListener('click', closure);

        chrome.runtime.sendMessage({action: 'kmPopupInit'});
	}

	bind(method) {
        var _this = this;
        return(function() {
            return(method.apply(_this, arguments));
        });
	}

	listener(request, sender, sendResponse) {
		if(typeof(request.cmd) === 'undefined') return;
        switch(request.cmd) {
            case 'bkLoadOptions':
				var account = request.account;
                this.setOptions(account.options);
                // request.state.opponent!
                if(account.state && account.state.opponent && account.state.opponent !== account.player) {
                    console.log('optionsPopup(opponent): ' + JSON.stringify(account.state));
                }
                console.log('optionsPopup: ' + JSON.stringify(request));
                break;
            default:
                console.log('optionsPopup: ' + JSON.stringify(request));
                break;
        }
	}

	setOptions(options) {
        Object.assign(this.options, options);

        document.getElementById('enableAccount').checked = options.Account.enabled;
        document.getElementById('enableOK').checked = options.OK.enabled;
        document.getElementById('enableOS').checked = options.OS.enabled;
        document.getElementById('enableWork').checked = options.Work.enabled;
        document.getElementById('enableGM').checked = options.GM.enabled;
        document.getElementById('enableMission').checked = options.Mission.enabled;
        document.getElementById('enableDuel').checked = options.Duel.enabled;
        document.getElementById('enableTournament').checked = options.Tournament.enabled;
        document.getElementById('enableTreasury').checked = options.Treasury.enabled;

        document.getElementById('bkOptions').style.display = 'block';
	}

	storeOptions() {
	}

}

function bkOptionsDialog() {
    this.options = {
        enableAccount: true,
        enableOK: false,
        enableOS: false,
        enableWork: false,
        enableGM: false,
        enableMission: false,
        enableDuel: false,
        enableTournament: false,
        enableTreasury: false
    };

    this.bind = function(method) {
        var _this = this;
        return(function() {
            return(method.apply(_this, arguments));
        });
    };

    this.setOptions = function(options) {
        //alert('bkOptions: ' + JSON.stringify(options));
        Object.assign(this.options, options);

        document.getElementById('enableAccount').checked = options.enableAccount;
        document.getElementById('enableOK').checked = options.enableOK;
        document.getElementById('enableOS').checked = options.enableOS;
        document.getElementById('enableWork').checked = options.enableWork;
        document.getElementById('enableGM').checked = options.enableGM;
        document.getElementById('enableMission').checked = options.enableMission;
        document.getElementById('enableDuel').checked = options.enableDuel;
        document.getElementById('enableTournament').checked = options.enableTournament;
        document.getElementById('enableTreasury').checked = options.enableTreasury;

        document.getElementById('bkOptions').style.display = 'block';
    };

    this.saveOptions = function() {
        this.options.enableAccount = document.getElementById('enableAccount').checked;
        this.options.enableOK = document.getElementById('enableOK').checked;
        this.options.enableOS = document.getElementById('enableOS').checked;
        this.options.enableWork = document.getElementById('enableWork').checked;
        this.options.enableGM = document.getElementById('enableGM').checked;
        this.options.enableMission = document.getElementById('enableMission').checked;
        this.options.enableDuel = document.getElementById('enableDuel').checked;
        this.options.enableTournament = document.getElementById('enableTournament').checked;
        this.options.enableTreasury = document.getElementById('enableTreasury').checked;

        chrome.runtime.sendMessage({action: 'kmPopupSave', options: this.options}, null);

        window.close(); // close the popup
    };

    // message handling
    this.listener = function(request, sender, sendResponse) {
		if(typeof(request.cmd) === 'undefined') return;
        switch(request.cmd) {
            case 'bkLoadOptions':
				var account = request.account;
                this.setOptions(account.options);
                // request.state.opponent!
                if(account.state && account.state.opponent && account.state.opponent !== account.player) {
                    //alert('popup.loadOptions: ' + JSON.stringify(account.state));
                }
                console.log('bkOptionsPopup: ' + JSON.stringify(request));
                break;
            default:
                console.log('bkOptionsPopup: ' + JSON.stringify(request));
                break;
        }
    };

    this.openSub = function(e) {
		var sub = e.target.id;
		var oDiv = document.getElementById('bkOptions');
		var oTable = oDiv.querySelector('table');
		var myDiv;
		switch(sub) {
			case 'optionsAccount':
				myDiv = document.getElementById('accountOptions');
				break;
			case 'optionsOK':
				myDiv = document.getElementById('okOptions');
				break;
			case 'optionsOS':
				myDiv = document.getElementById('osOptions');
				break;
			default:
				break;
		}
		if(!myDiv) return;
		myDiv.style.top = oDiv.offsetTop + 'px';
		myDiv.style.width = oDiv.clientWidth + 15 + 'px';
		myDiv.style.height = oTable.clientHeight + 'px';
		//console.log('openSub(' + sub + '): ' + oDiv.offsetTop + ' :: ' + oDiv.clientWidth + ', ' + document.body.style.width);
	};

	this.closeSub = function(e) {
		var pDiv = e.target;
		while(pDiv.nodeName !== 'DIV') pDiv = pDiv.parentElement;
		//console.log('closeSub(' + pDiv.nodeName + '): ');
		if(!pDiv) return;
		pDiv.style.width = 0;
		pDiv.style.height = 0;
		pDiv.style.top = 0;
	};

    // initializer
    this.initialize = function() {
        var closure = this.bind(this.listener);
        chrome.runtime.onMessage.addListener(closure);

        closure = this.bind(this.saveOptions);
        document.getElementById('saveButton').addEventListener('click', closure);

		closure = this.bind(this.openSub);
        var subs = document.getElementsByClassName('subOpen');
        for(var i = 0; i < subs.length; ++i) {
			subs[i].addEventListener('click', closure);
		}

		closure = this.bind(this.closeSub);
        var subs = document.getElementsByClassName('subClose');
        for(var i = 0; i < subs.length; ++i) {
			subs[i].addEventListener('click', closure);
		}

        chrome.runtime.sendMessage({action: 'kmPopupInit'});
    };

    this.initialize();
}

var bk_options = new bkOptionsDialog();
