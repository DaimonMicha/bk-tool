class kmManager extends kmObject {

    constructor() {
		super();
		this.servers = new Map();
		this.accounts = new Map();
	    chrome.runtime.onMessage.addListener(this.bind(this.listener));
	}

	listener(request, sender, sendResponse) {
		if(typeof(request.action) === 'undefined') return(false);

		console.log('kmManager::listener(' + JSON.stringify(request.account) + ') ' + request.action);

		switch(request.action) {

			case 'kmContentInit':
				var account = this.getAccount(request.account);
                var error = this.injectContent(sender.tab.id);
                if(error) {
                    console.log('inject return an error: ' + chrome.runtime.lastError.message);
                }
		        sendResponse({result: 'success', account: account, error: error});
				break;

			case 'kmSaveState':
				var account = this.storeState(request);
		        sendResponse({result: 'success', account: account});
				break;

			case 'kmPopupInit':
				this.popupInit(sender.id);
				break;

			case 'kmPopupSave':
				this.storeOptions(request.options);
				break;

			case 'bkGetKnight':
				var knight = this.getKnight(request);
		        sendResponse({result: 'success', knight: knight, request: request});
				break;

			case 'bkSetKnight':
				this.setKnight(request);
				break;

			case 'bkStoreServer':
				this.storeServer(request);
				break;

			case 'bkCheckReports':
				var result = this.checkReports(request);
		        sendResponse({result: 'success', reports: result});
				break;

			case 'bkCheckActions':
				var result = this.checkActions(request);
				sendResponse(result);
				break;

			default:
				break;

		}
		return(false);

	}

    // storage handling
    storeData(key, value) {
        try {
            localStorage[key] = JSON.stringify(value);
            return true;
        } catch(e) {
            return false;
        }
    }

    loadData(key) {
        try {
            var value = localStorage[key];
            return value ? JSON.parse(value) : null;
        } catch(e) {
			return null;
		}
    }

    removeData(key) {
        try {
            delete localStorage[key];
            return true;
        } catch(e) {
            return false;
        }
    }

	// popup handling
	popupInit(sender) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            var exurl = /^(http|https):\/\/s[0-9]{1,2}-de.battleknight.gameforge.com\/*/g;
            var url = tabs[0].url;
            if(url.match(exurl)) {
                chrome.tabs.sendMessage(tabs[0].id, {action: 'kmGetAccount'}, function(response) {
                    if(response) {
						console.log('kmManager::popupInit ' + JSON.stringify(response));
                        chrome.runtime.sendMessage(sender, {cmd: 'bkLoadOptions', account: response.account});
                    }
					if(chrome.runtime.lastError)
						console.log(chrome.runtime.lastError.message);
                });
                if(chrome.runtime.lastError)
					console.log(chrome.runtime.lastError.message);
            }
        });
	}

	// content inject hack
    injectContent(tabId) {
        var resourceUrl = chrome.runtime.getURL('/content/pageinjector.js');
        const actualCode = `
            var s = document.createElement('script');
            s.src = '${resourceUrl}';
            s.onload = function() {
                this.remove();
            };
            (document.head || document.documentElement).appendChild(s);
        `;
        chrome.tabs.executeScript(tabId, {code: actualCode, runAt: 'document_idle'});
        return(chrome.runtime.lastError);
	}

	// helper methods
    initAccount(acc) {
		var req_key = acc.server + '/' + acc.player;
		var val = this.loadData(req_key + '-options');
		acc.options = val ? new kmOptions(val) : new kmOptions();
		val = this.loadData(req_key + '-state');
		acc.state = val ? new bkState(val) : new bkState();
		acc = new kmAccount(acc);
		//console.log('initAccount ' + JSON.stringify(acc));
		return(acc);
	}

	updateTabsOptions(account) {
		var exurl = '*://' + account.server + '.battleknight.gameforge.com/*';
		var options = account.options;
		chrome.tabs.query({url: exurl}, function(tabs) {
			for(let tab of tabs) {
				chrome.tabs.sendMessage(tab.id, {action: 'kmUpdateOptions', options: options}, null);
			}
		});
	}

	updateTabsStates(account) {
		var exurl = '*://' + account.server + '.battleknight.gameforge.com/*';
		var state = account.state;
		chrome.tabs.query({url: exurl}, function(tabs) {
			for(let tab of tabs) {
				chrome.tabs.sendMessage(tab.id, {action: 'kmUpdateState', state: state}, null);
			}
		});
	}

	// message handling
    getAccount(acc) {
		var req_key = acc.server + '/' + acc.player;
		if(this.accounts.has(req_key)) {
			return(this.accounts.get(req_key));
		}

		acc = this.initAccount(acc);
		this.accounts.set(req_key, acc);
		return(acc);
	}

	storeOptions(options) {
		var _this = this;
		var acc = {};
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {action: 'kmGetAccount'}, function(response) {
				if(response) {
					acc = response.account;
					acc = _this.getAccount(acc);
					Object.assign(acc.options, options);
					var req_key = acc.server + '/' + acc.player;
					_this.accounts.set(req_key, acc);
					_this.storeData(req_key + '-options', options);
					_this.updateTabsOptions(acc);
				}
                if(chrome.runtime.lastError)
					console.log(chrome.runtime.lastError.message);
			});
        });
	}

	storeState(request) {
		//console.log('storeState ' + JSON.stringify(request.state));
		var acc = this.getAccount(request.account);
		Object.assign(acc.state, request.state);
		var req_key = acc.server + '/' + acc.player;
		this.storeData(req_key + '-state', acc.state);
		this.updateTabsStates(acc);
		return(acc);
	}

	getKnight(request) {
		//console.log('kmManager::getKnight ' + JSON.stringify(request));

		var server = this.getServer(request.account.server);
		var knight = server.getKnight(parseInt(request.knight_id));
		return(knight);
	}

	setKnight(request) {
		//console.log('kmManager::setKnight ' + JSON.stringify(request));

		var server = this.getServer(request.account.server);
		server.setKnight(request.knight);
		//this.servers.set(request.account.server, server);
	}

	getServer(world) {
		if(this.servers.has(world)) {
			return(this.servers.get(world));
		}

		var server = new kmServer({world: world});
		var req_key = server.name;
		var val = this.loadData(req_key + '-knights') || [];

		val.forEach(function(value) {
			server.setKnight(value);
		});

		this.servers.set(world, server);
		console.log('kmManager::getServer ' + JSON.stringify(server) + ', ' + val.length);
		return(server);
	}

	storeServer(request) {
		var server = this.getServer(request.account.server);
		var knights = [];
		server.knights.forEach(function(value, key, map) {
			knights.push(value);
		});
		var req_key = server.name;
		this.storeData(req_key + '-knights', knights);

		// debug
		console.log('kmManager::storeServer ' + server.name + ', '
			+ knights.length + ' Knights, '
			+ server.reports.size + ' Reports.'
		);
	}

	checkReports(request) {
		//console.log('checkReports ' + JSON.stringify(request.account) + ', ' + request.reports.length);
		// bkAccount liest den report
		// bkServer speichert die letzten 200 reports oder so.
		var mails = request.reports;
		var account = this.getAccount(request.account);
		var reports = account.checkReports(mails, this.getServer(account.server));
		//console.log('kmManager::checkReports: ' + JSON.stringify(reports));
		this.storeServer({account: account});
		return(reports);
	}

	checkActions(request) {
		var account = this.getAccount(request.account);
		var action = {command: 'nothing'};

		//if(account.hasCooldown()) {
			// passive checks
			// toDo:
			// OS-Check, OS-Fight
			// Joust-Check
			//return;
		//} else {
			// aktive checks
			// ohne cooldown
			// Work, GM, Mission, Duell
			//if(this.options.enableWork) this.checkWorkAction();
		//}

		if(account.options.enableAccount) {
			if(!account.state.currentAction) account.state.currentAction = account.findAction();
			if(account.state.currentAction) {
				var worker;

				switch(account.state.currentAction) {
					case 'Work':
						worker = new workAction(account, this.getServer(account.server));
						break;
					case 'OS':
						worker = new osAction(account, this.getServer(account.server));
						break;
					case 'GM':
						worker = new gmAction(account, this.getServer(account.server));
						break;
					default:
						break;
				}

				if(worker) {
					var cmd = worker.check(account.hasCooldown());
					if(cmd && typeof(cmd) === 'object') {
						Object.assign(action, cmd);
					}
				}
				console.log('kmManager::checkActions(' + JSON.stringify(request.account) + ') action = ' + JSON.stringify(action) + ', cooldown: ' + account.hasCooldown());
			}
		}

		return({result: 'success', action: action});
	}

}
