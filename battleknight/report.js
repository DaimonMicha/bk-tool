class bkReport {

	// report.keys["mail_id","mail_type","mail_sender","mail_receiver",
	// "mail_subject","mail_content","mail_receiver_status","mail_sender_status",
	// "mail_senttime","recipient_name","recipient_enemy_defeated",
	// "recipient_gender","knight_id","mail_html"]
	constructor({
		mail_id = 0,
		mail_type = 0,
		mail_sender = '',
		mail_receiver = '',
		mail_subject = '',
		mail_content = {},
		mail_receiver_status = '',
		mail_sender_status = '',
		mail_senttime = '',
		recipient_name = '',
		recipient_enemy_defeated = '',
		recipient_gender = '',
		knight_id = 0
	} = {}) {
		Object.assign(this, {
			mail_id,
			mail_type,
			mail_sender,
			mail_receiver,
			mail_subject,
			mail_content,
			mail_receiver_status,
			mail_sender_status,
			mail_senttime,
			recipient_name,
			recipient_enemy_defeated,
			recipient_gender,
			knight_id
		});
		// (mailType == 0) Kampfbericht / OS - Als Söldner angeheuert. / OS - Angriffsphase / Lanzenstechen - Turnierstart
		// (mailType == 2) Ordensnachricht
		// (mailType == 3) Ordensschlacht
		switch(parseInt(mail_type)) {
			case 0:
				this.parseFightReport();
				break;
			case 3:
				console.log('bkReport(' + this.mail_id + ') ' + Object.keys(mail_content)); //JSON.stringify(stats));
				break;
			default:
				delete(this.mail_content);
				break;
		}
		this.mail_senttime = Date.parse(this.mail_senttime) / 1000;
	}

    // mail_content.keys["fight_id","aggressor_id","defender_id","aggressor","defender","fight_time","fight_log","fight_stats","fight_type"]
    // "" = Ordensschlacht
    //("war_attacker", "war_castle", "war_defender", "war_endtime", "war_fight_data", "war_id", "war_initiator", "war_looted_silver", "war_payment_data", "war_result", "war_rounds", "war_starttime", "war_type")
    // fight_type("duel") = Duell
    // fight_type("warfight") = Kriegsduell
    // fight_type("mob") = Mission
    // fight_type("group") = Gruppenmission
    // fight_type("joust") = Turnier
	parseFightReport() {
		var content = this.mail_content;
		switch(content.fight_type) {
			case 'duel':
			case 'joust':
			case 'warfight':
				this.mail_content.fight_time = Date.parse(content.fight_time) / 1000;
				this.mail_content.fight_log = unserialize(content.fight_log);;
				this.mail_content.fight_stats = unserialize(content.fight_stats);;
				this.mail_content.aggressor = unserialize(content.aggressor.replace("O:13:\"FighterObject\":","a:"));
				this.mail_content.defender = unserialize(content.defender.replace("O:13:\"FighterObject\":","a:"));
				break;
			default:
				delete(this.mail_content);
				break;
		}
/*
		console.log('bkReport::parseFightReport(' + this.mail_id + ') '
			+ 'type(' + content.fight_type + ') '
			+ Object.keys(content)); //JSON.stringify(stats));
*/
	}

	get is_fightReport() {
		if(!this.mail_content)	return(false);
		if(!this.mail_content.fight_id) return(false);
		return(true);
	}

	get short() {
		if(!this.is_fightReport) return(this);
		var copy = this;
		var stats = this.mail_content.fight_stats;
		for(let fighter of [this.mail_content.aggressor, this.mail_content.defender]) {
		}
		var fighter = this.mail_content.aggressor;
		stats.aggressor.id = fighter.id;
		stats.aggressor.name = fighter.name;
		stats.aggressor.hitZones = fighter.hitZones;
		stats.aggressor.defendZones = fighter.defendZones;

		fighter = this.mail_content.defender;
		stats.defender.id = fighter.id;
		stats.defender.name = fighter.name;
		stats.defender.hitZones = fighter.hitZones;
		stats.defender.defendZones = fighter.defendZones;

		//console.log('bkReport::short ' + Object.keys(stats.aggressor));

		stats.fight_type = this.mail_content.fight_type;
		copy.mail_content = stats;
		return(copy);
	}

}
