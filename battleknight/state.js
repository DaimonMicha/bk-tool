class bkState extends kmObject {
	constructor({
		Account = {
			cooldown: {},
			karma: {},
			player: {},
			premium: {
			},
			clues: []
		},
		OK = {
			castle: false
		},
		OS = {},
		Work = {
			actionsEnabled: true
		},
		GM = {},
		Mission = {
			actionsEnabled: false
		},
		Duel = {
			actionsEnabled: false,
			opponents: []
		},
		Tournament = {},
		Treasury = {},
		Reports = {},
		currentAction = '',
		path = '',
		query = ''
	} = {}) {
		super();
		Object.assign(this, {
			Account,
			OK,
			OS,
			Work,
			GM,
			Mission,
			Duel,
			Tournament,
			Treasury,
			Reports,
			currentAction,
			path,
			query
		});
	}
}
