class kmUI extends kmObject {

	constructor(account) {
		super();
        var pDiv = this.injectUI();
        this.informant = document.getElementById('km_informant');
        this.readyRead = document.getElementById('km_readyRead');
        this.readyWrite = document.getElementById('km_readyWrite');
        this.responseReady = document.getElementById('km_responseReady');

        var closure = this.bind(this.progressbarUpdater);
        window.setInterval(closure, 1000);
	}

	injectUI() {
        var resourceUrl = chrome.runtime.getURL('klickmeister.png');
        var pDiv = document.getElementById('km_accountPlugin');
        if(pDiv) return(pDiv);
        pDiv = document.createElement('div');
        pDiv.setAttribute('id', 'km_accountPlugin');
        pDiv.innerHTML = `
            <div id='km_informant' class='nodisplay'>
                <input type='checkbox' id='km_readyRead'></input>
                <input type='checkbox' id='km_readyWrite'></input>
                <input type='checkbox' id='km_responseReady'></input>
            </div>
            <div id="bkAccountModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th colspan="2" id="klickMeisterLogo"><img src="${resourceUrl}" width="135px"></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkOKModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th></th><th><a id="bkOKLink" href="/clan/upgrades">Ordenskasse</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkOSModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input id="km_enableOSActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/clanwar/battle">Ordenskrieg</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkWorkModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input id="km_enableWorkActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/market/work">Arbeit</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkGMModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input id="km_enableGMActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/groupmission/group/">GruppenMission</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkMissionModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input disabled="disabled" id="km_enableMissionActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/world/location">Mission</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkDuelModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input disabled="disabled" id="km_enableDuelActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/duel/">Duell</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkTournamentModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input disabled="disabled" id="km_enableTournamentActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/joust/">Turnier</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="bkTreasuryModule" class="kmTopic nodisplay">
                <table>
                    <thead>
                        <tr><th><input disabled="disabled" id="km_enableTreasuryActions" class="kmChecker kmOptions" type="checkbox" title="mach mich an!"></input></th><th><a href="/treasury">Schatzkammer</a></th></tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <!-- ModulTemplate -->
            <div id="bkModuleTemplate" class="kmTopic nodisplay">
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div> <!-- #ModulTemplate -->
        `;
        document.body.appendChild(pDiv);
        return(pDiv);
	}

	addPlayerZones(opponent) {
		var text = 'Trefferzonen von ' + opponent.rang + ' ' + opponent.name;
		var main = document.getElementById('mainContent');
        var pDiv = document.createElement('br');
        pDiv.setAttribute('class', 'clearfloat');

        pDiv = document.createElement('div');
        pDiv.setAttribute('class', 'paperTabs');
        pDiv.innerHTML = `
        <div class="onetab" id="tabsnavi">
				<ul class="clearfix">
					<li class="static">
						<span class="tabLeft"></span>
						<span class="tabMiddle">${text}</span>
						<span class="tabRight"><input id="km_enableDuel" class="kmChecker" type="checkbox" title="duelliere mich!"></input></span>
					</li>
				</ul>
				<br class=""/>
			</div>

        `;
        pDiv.setAttribute('style', 'margin-top: -20px;');
        main.appendChild(pDiv);
        pDiv = document.createElement('div');
        pDiv.setAttribute('class', 'paperContainer');
        pDiv.innerHTML = `
        <div id="targetzones" class="paperContainerInner">
				<ul id="zonesIndicateRound">
					<li><h5>Runde 1</h5>
				<p class="zonesIndicateHit">Schlag 1</p>
				<p class="zonesIndicateBlock">Block 1</p>
					</li>
					<li><h5>Runde 2</h5>
				<p class="zonesIndicateHit">Schlag 2</p>
				<p class="zonesIndicateBlock">Block 2</p>
					</li>
					<li><h5>Runde 3</h5>
				<p class="zonesIndicateHit">Schlag 3</p>
				<p class="zonesIndicateBlock">Block 3</p>
					</li>
					<li><h5>Runde 4</h5>
				<p class="zonesIndicateHit">Schlag 4</p>
				<p class="zonesIndicateBlock">Block 4</p>
					</li>
					<li><h5>Runde 5</h5>
				<p class="zonesIndicateHit">Schlag 5</p>
				<p class="zonesIndicateBlock">Block 5</p>
					</li>
				</ul>

			<div id="targetzonesAttack">
				<div id="targetAreaAttack01" class="targetAreaContainer">
					<a id="targetzonetip01_1" class="topLeftAttack toolTip targetZone1"></a>
					<a id="targetzonetip01_2" class="topMidAttack toolTip targetZone2"></a>
					<a id="targetzonetip01_3" class="topRightAttack toolTip targetZone3"></a>
					<a id="targetzonetip01_4" class="midLeftAttack toolTip targetZone4"></a>
					<a id="targetzonetip01_5" class="midCentreAttack toolTip targetZone5"></a>
					<a id="targetzonetip01_6" class="midRightAttack toolTip targetZone6"></a>
					<a id="targetzonetip01_7" class="bottomLeftAttack toolTip targetZone7"></a>
					<a id="targetzonetip01_8" class="bottomMidAttack toolTip targetZone8"></a>
					<a id="targetzonetip01_9" class="bottomRightAttack toolTip targetZone9"></a>
				</div>
				<div id="targetAreaAttack02" class="targetAreaContainer">
					<a id="targetzonetip02_1" class="topLeftAttack toolTip targetZone1"></a>
					<a id="targetzonetip02_2" class="topMidAttack toolTip targetZone2"></a>
					<a id="targetzonetip02_3" class="topRightAttack toolTip targetZone3"></a>
					<a id="targetzonetip02_4" class="midLeftAttack toolTip targetZone4"></a>
					<a id="targetzonetip02_5" class="midCentreAttack toolTip targetZone5"></a>
					<a id="targetzonetip02_6" class="midRightAttack toolTip targetZone6"></a>
					<a id="targetzonetip02_7" class="bottomLeftAttack toolTip targetZone7"></a>
					<a id="targetzonetip02_8" class="bottomMidAttack toolTip targetZone8"></a>
					<a id="targetzonetip02_9" class="bottomRightAttack toolTip targetZone9"></a>
				</div>
				<div id="targetAreaAttack03" class="targetAreaContainer">
					<a id="targetzonetip03_1" class="topLeftAttack toolTip targetZone1"></a>
					<a id="targetzonetip03_2" class="topMidAttack toolTip targetZone2"></a>
					<a id="targetzonetip03_3" class="topRightAttack toolTip targetZone3"></a>
					<a id="targetzonetip03_4" class="midLeftAttack toolTip targetZone4"></a>
					<a id="targetzonetip03_5" class="midCentreAttack toolTip targetZone5"></a>
					<a id="targetzonetip03_6" class="midRightAttack toolTip targetZone6"></a>
					<a id="targetzonetip03_7" class="bottomLeftAttack toolTip targetZone7"></a>
					<a id="targetzonetip03_8" class="bottomMidAttack toolTip targetZone8"></a>
					<a id="targetzonetip03_9" class="bottomRightAttack toolTip targetZone9"></a>
				</div>
				<div id="targetAreaAttack04" class="targetAreaContainer">
					<a id="targetzonetip04_1" class="topLeftAttack toolTip targetZone1"></a>
					<a id="targetzonetip04_2" class="topMidAttack toolTip targetZone2"></a>
					<a id="targetzonetip04_3" class="topRightAttack toolTip targetZone3"></a>
					<a id="targetzonetip04_4" class="midLeftAttack toolTip targetZone4"></a>
					<a id="targetzonetip04_5" class="midCentreAttack toolTip targetZone5"></a>
					<a id="targetzonetip04_6" class="midRightAttack toolTip targetZone6"></a>
					<a id="targetzonetip04_7" class="bottomLeftAttack toolTip targetZone7"></a>
					<a id="targetzonetip04_8" class="bottomMidAttack toolTip targetZone8"></a>
					<a id="targetzonetip04_9" class="bottomRightAttack toolTip targetZone9"></a>
				</div>
				<div id="targetAreaAttack05" class="targetAreaContainer">
					<a id="targetzonetip05_1" class="topLeftAttack toolTip targetZone1"></a>
					<a id="targetzonetip05_2" class="topMidAttack toolTip targetZone2"></a>
					<a id="targetzonetip05_3" class="topRightAttack toolTip targetZone3"></a>
					<a id="targetzonetip05_4" class="midLeftAttack toolTip targetZone4"></a>
					<a id="targetzonetip05_5" class="midCentreAttack toolTip targetZone5"></a>
					<a id="targetzonetip05_6" class="midRightAttack toolTip targetZone6"></a>
					<a id="targetzonetip05_7" class="bottomLeftAttack toolTip targetZone7"></a>
					<a id="targetzonetip05_8" class="bottomMidAttack toolTip targetZone8"></a>
					<a id="targetzonetip05_9" class="bottomRightAttack toolTip targetZone9"></a>
				</div>
			</div>

			<div id="targetzonesDefend">
				<div id="targetAreaDefend01" class="targetAreaContainer defend">
					<a id="shieldZone01_1" class="topLeftDefend toolTip defendZone1"></a>
					<a id="shieldZone01_2" class="topRightDefend toolTip defendZone2"></a>
					<a id="shieldZone01_3" class="midCentreDefend toolTip defendZone3"></a>
					<a id="shieldZone01_4" class="bottomLeftDefend toolTip defendZone4"></a>
					<a id="shieldZone01_5" class="bottomRightDefend toolTip defendZone5"></a>
				</div>
				<div id="targetAreaDefend02" class="targetAreaContainer defend">
					<a id="shieldZone02_1" class="topLeftDefend toolTip defendZone1"></a>
					<a id="shieldZone02_2" class="topRightDefend toolTip defendZone2"></a>
					<a id="shieldZone02_3" class="midCentreDefend toolTip defendZone3"></a>
					<a id="shieldZone02_4" class="bottomLeftDefend toolTip defendZone4"></a>
					<a id="shieldZone02_5" class="bottomRightDefend toolTip defendZone5"></a>
				</div>
				<div id="targetAreaDefend03" class="targetAreaContainer defend">
					<a id="shieldZone03_1" class="topLeftDefend toolTip defendZone1"></a>
					<a id="shieldZone03_2" class="topRightDefend toolTip defendZone2"></a>
					<a id="shieldZone03_3" class="midCentreDefend toolTip defendZone3"></a>
					<a id="shieldZone03_4" class="bottomLeftDefend toolTip defendZone4"></a>
					<a id="shieldZone03_5" class="bottomRightDefend toolTip defendZone5"></a>
				</div>
				<div id="targetAreaDefend04" class="targetAreaContainer defend">
					<a id="shieldZone04_1" class="topLeftDefend toolTip defendZone1"></a>
					<a id="shieldZone04_2" class="topRightDefend toolTip defendZone2"></a>
					<a id="shieldZone04_3" class="midCentreDefend toolTip defendZone3"></a>
					<a id="shieldZone04_4" class="bottomLeftDefend toolTip defendZone4"></a>
					<a id="shieldZone04_5" class="bottomRightDefend toolTip defendZone5"></a>
				</div>
				<div id="targetAreaDefend05" class="targetAreaContainer defend">
					<a id="shieldZone05_1" class="topLeftDefend toolTip defendZone1"></a>
					<a id="shieldZone05_2" class="topRightDefend toolTip defendZone2"></a>
					<a id="shieldZone05_3" class="midCentreDefend toolTip defendZone3"></a>
					<a id="shieldZone05_4" class="bottomLeftDefend toolTip defendZone4"></a>
					<a id="shieldZone05_5" class="bottomRightDefend toolTip defendZone5"></a>
				</div>
			</div>

		</div>
        `;
        pDiv.setAttribute('style', 'margin-left: -15px;');
        main.appendChild(pDiv);

        this.informant.setAttribute('what', 'bk_zones');
        this.responseReady.click();

        for(var i = 0;i < opponent.hitZones.length; ++i) {
			var zid = 'targetzonetip0' + (i+1) + '_' + opponent.hitZones[i];
			document.getElementById(zid).classList.add('active');
		}
        for(var i = 0;i < opponent.defendZones.length; ++i) {
			var zid = 'shieldZone0' + (i+1) + '_' + opponent.defendZones[i];
			document.getElementById(zid).classList.add('active');
		}
	}

    timeString(timestamp) {
        var hours = Math.abs(parseInt(timestamp / 3600)).toFixed(0) || 0;
        var minutes = Math.abs(parseInt((timestamp % 3600) / 60)).toFixed(0) || 0;
        var seconds = Math.abs(timestamp - hours * 3600 - minutes * 60).toFixed(0) || 0;
        if(hours < 10)   hours = '0' + hours.toString();
        if(minutes < 10) minutes = '0' + minutes.toString();
        if(seconds < 10) seconds = '0' + seconds.toString();
        return({
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        });
    }

	updateOptions(options) {
        var pDiv = document.getElementById('km_accountPlugin');

        var modules = pDiv.querySelectorAll('div.kmTopic');
        var accDiv = pDiv.querySelector('#bkAccountModule');

        if(!options.enableAccount) {
			for(let module of modules) {
                module.classList.add('nodisplay');
            }

		} else {
            if(accDiv) accDiv.classList.remove('nodisplay');
			for(let module of modules) {
                var id = module.getAttribute('id');
                switch(id) {
					case 'bkAccountModule':
                        if(options.enableAccount) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkOKModule':
                        if(options.enableOK) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkOSModule':
                        if(options.enableOS) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkWorkModule':
                        if(options.enableWork) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkGMModule':
                        if(options.enableGM) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkMissionModule':
                        if(options.enableMission) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkDuelModule':
                        if(options.enableDuel) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkTournamentModule':
                        if(options.enableTournament) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    case 'bkTreasuryModule':
                        if(options.enableTreasury) module.classList.remove('nodisplay');
                        else module.classList.add('nodisplay');
                        break;
                    default:
                        break;
                }
            }
		}

	}

    appendLine(module) {
        //check auf doppelte id!
        var target = document.getElementById(module);
        if(!target) return;

        target = target.querySelector('tbody');
        target.innerHTML += "<tr><td id=\"_appendLine_\" colspan=\"2\" align=\"center\"></td></tr>";
        var insert = target.querySelector("#_appendLine_");
        insert.removeAttribute('id');
        return(insert);
    }

    insertLine(module) {
        var target = document.getElementById(module);
        if(!target) return;

        target = target.querySelector('tbody');
        var cnt = target.innerHTML;
        target.innerHTML = "<tr><td id=\"_appendLine_\" colspan=\"2\" align=\"center\"></td></tr>" + cnt;
        var insert = target.querySelector("#_appendLine_");
        insert.removeAttribute('id');
        return(insert);
	}

    addProgressBar(module, id, text, endTime, duration) {
        //check auf doppelte id!
        var target = document.getElementById('progressbarText' + id);
        if(target) return(target);

        //if(typeof text === 'undefined') text = '';
        if(text !== '') text += ': ';
        target = this.insertLine(module);
        target.innerHTML += `
            <div id="progressbarText${id}" class="km_progressbar" style="position: relative; width: 100%; height: 18px;">
                <div align="left" id="timeMeter${id}" class="kmProgressMeter" style="width: 0%; height: 16px; position: absolute; background-color: rgba(255, 0, 0, 0.35);"></div>
                <div class="km_progressText" style="display: inline; padding-right: 5px;">${text}</div><span></span>
            </div>
        `;
        var progressDiv = target.querySelector('div.km_progressbar');
        progressDiv.setAttribute("km_timer", "on");
        progressDiv.setAttribute('km_endTime', endTime);
        progressDiv.setAttribute('km_duration', duration);
        return(target);
    }

    removeProgressRow(progressDiv) {
		progressDiv = document.getElementById(progressDiv);
		while(progressDiv.nodeName !== 'TR') {
			progressDiv = progressDiv.parentElement;
		}
		if(!progressDiv) return;
		progressDiv.parentElement.removeChild(progressDiv);

		//console.log('removeProgressRow(' + progressDiv.nodeName + ')');
	}

	progressbarUpdater() {
        var target = document.getElementById('km_accountPlugin');
        var modules = target.querySelectorAll('div.km_progressbar');
        var cTime = parseInt(Date.now() / 1000).toFixed(0);
        for(let module of modules) {
			var progressbarWidth = 0;
			var progressMeter = module.querySelector('div.kmProgressMeter');
			if(module.getAttribute('km_timer') === 'on') {
				var end = module.getAttribute('km_endTime');
				if(end <= cTime) {
					this.informant.setAttribute('what', 'bk_timeout');
					this.informant.setAttribute('data', module.getAttribute('id'));
					this.readyWrite.click();
					continue;
				}
				var eta = (end - cTime);
				var duration = module.getAttribute('km_duration');
				var values = this.timeString(eta);
				var text = values.hours + ':' + values.minutes + ':' + values.seconds;
				module.querySelector('span').innerHTML = text;

				var dir = 'up';
				if(module.hasAttribute('km_direction') && module.getAttribute('km_direction') === 'down') {
					dir = 'down';
				}
				if(dir === 'down') progressbarWidth = parseInt((100 / duration) * eta);
				else progressbarWidth = (100 - parseInt((100 / duration) * eta));

			} else {

				var haben = module.getAttribute('km_endTime');
				var soll = module.getAttribute('km_duration');
				var text = '';
				if(haben === 'undefined' || soll === 'undefined') {
					text = 'unbekannt';
				} else {
					haben = parseInt(haben);
					soll = parseInt(soll);
					if(haben <= soll) {
						progressbarWidth = parseInt((100 / soll) * haben);
						text = 'fehlen noch.';
						module.querySelector('div.km_progressText').innerHTML = (soll - haben);
					}
				}
				module.querySelector('span').innerHTML = text;
			}
            progressMeter.style.width = progressbarWidth + '%';
        }
	}

	updateState(state) {
		//console.log('kmUI::updateState ' + JSON.stringify(state));
	}

}
