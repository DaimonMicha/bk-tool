class kmKlickMeister extends kmObject {

    constructor() {
		super();
        var pDiv = document.getElementById('shieldNeutral');
        if(!pDiv) {
            console.log('KlickMeister: nicht im Spiel! ＼(＾O＾)／');
            return;
        }

        this.options = new bkOptions();
        this.state = new bkState();
        this.server = window.location.hostname.split('.')[0];
        this.player = parseInt(this.splitPath(pDiv.href)[4]);

        this.ui = new kmUI(this);
        this.ui.readyRead.addEventListener('change', this.bind(this.informantResponse));
        this.ui.readyWrite.addEventListener('change', this.bind(this.informantResponse));

        window.addEventListener('unload', this.bind(this.saveState));

        var request = {action: 'kmContentInit', account: {server: this.server, player: this.player}};
        chrome.runtime.sendMessage(request, this.bind(this.initResponse));
	}

	splitPath(path) {
        var paths = [];
        var splitter = path.split('/');
        splitter.forEach(function(item) {
            if(item !== '') paths.push(item);
        });
        return(paths);
	}

	splitSearch(line, key) {
		if(!line) return;
        var gebastel = line.substr(1);
        gebastel = gebastel.split('&');
        for(var i = 0; i < gebastel.length; ++i) {
            var items = gebastel[i].split('=');
            if(items[0] === key) {
                return(items[1]);
            }
        }
    }

	listener(request, sender, sendResponse) {
		if(typeof(request.action) === 'undefined') return(false);
		var action = request.action || 'none';
		console.log('kmKlickMeister::listener(' + action + ')');

		switch(action) {
			case 'kmGetAccount':
		        sendResponse({result: 'success', account: {server: this.server, player: this.player, options: this.options}});
				break;

			case 'kmUpdateOptions':
				this.updateOptions(request.options);
				break;

			case 'kmUpdateState':
				this.ui.updateState(request.state);
				if(this.options.enableDuel) {
					if(typeof(this.renderDuel) === 'undefined') {
						Object.assign(this, new kmRenderer());
					}
					this.renderDuel();
				}
				break;

			default:
				//sendResponse({result: 'warning', description: 'command not known!'});
				break;
		}

		return(false);
	}

    initResponse(response) {
		if(response.result !== 'success') return(false);

		Object.assign(this.options, response.account.options);

		this.state = new bkState(response.account.state);
        this.state.path = window.location.pathname;
        this.state.query = window.location.search;

		console.log('initResponse: ' + JSON.stringify(this.options));

		this.ui.updateOptions(this.options);

        this.action_delay = randInt(376, 14867);

		// Hack, um Teile der class in eine andere
		// Datei auszulagern.
        Object.assign(this, new kmParser());

        chrome.runtime.onMessage.addListener(this.bind(this.listener));
        setTimeout(this.bind(this.getVars), 100);
        setTimeout(this.bind(this.checkActions), this.action_delay);
	}

	getVars() {
        this.ui.informant.setAttribute('what', 'bk_vars');
        this.ui.responseReady.click();
	}

	informantResponse() {
        var what = this.ui.informant.getAttribute('what');
        var data = this.ui.informant.getAttribute('data');

        switch(what) {
            case 'bk_vars':
				console.log('informantResponse(' + what + '): ' + JSON.stringify(this.state.Account));
				this.checkVars(JSON.parse(data));
				this.parsePage();
				this.renderPage();
				//this.saveState();
				break;

			case 'bk_reports':
				this.readReports(JSON.parse(data));
				//this.saveState();
				break;

			case 'bk_travel':
				this.checkTravel(JSON.parse(data));
				//this.saveState();
				break;

			case 'bk_mission':
				this.checkMission(JSON.parse(data));
				//this.saveState();
				break;

			case 'bk_inventory':
				this.checkInventory(JSON.parse(data));
				break;

			case 'bk_lootbox':
				this.checkLootbox(JSON.parse(data));
				break;

			case 'bk_timeout':
				this.checkTimeout(data);
				break;

			default:
				console.log('kmKlickMeister::informantResponse(' + what + '): ' + typeof(data) + ', ' + data);
				break;
		}
	}

	checkLootbox(data) {
		for(let it of data) {
			var item = new bkItem(it);
			console.log('checkLootbox ' + JSON.stringify(item.dump()));
		}
	}

	checkInventory(data) {
		if(!data.result) return;
		var data = data.items;
		for(let it of data) {
			var item = new bkItem(it);
			if(!item.clue_item) continue;
			console.log('checkInventory ' + JSON.stringify(item.dump()));
		}

		//console.log('checkInventory ' + JSON.stringify(data));
	}

	checkTravel(data) {
        var state = this.state.Account.cooldown || {};
        state.data = {};
        state.data.where = data["0"];
        state.data.how = data["1"];

		//console.log('checkTravel ' + JSON.stringify(data));
		this.state.Account.cooldown = state;

		state = this.state.Mission || {};
		//state.art = data['0'];
		//state.id = data['1'];
		//state.karma = data['2'];
		this.state.Mission = state;
		console.log('checkTravel ' + JSON.stringify(data));
	}

	checkMission(data) {
        var state = this.state.Account.cooldown || {};
        state.data = {};
        state.data.missionArt = data["0"];
        state.data.missionID = data["1"];
        state.data.missionKarma = data["2"];

		this.state.Account.cooldown = state;

		state = this.state.Mission || {};
		state.art = data['0'];
		state.id = data['1'];
		state.karma = data['2'];
		this.state.Mission = state;
		console.log('checkMission ' + JSON.stringify(state));
	}

	checkVars(vars) {
        var state = this.state.Account;
        state.player = vars.player || {};

		delete(state.abilities);
		delete(state.health);
		delete(state.maxHealth);

        this.state.Account = state;
		this.state.Server = vars.server || {};
		console.log('kmKlickMeister::checkVars ' + typeof(vars) + ': ' + JSON.stringify(vars));
	}

	updateOptions(options) {
		Object.assign(this.options, options);
		this.ui.updateOptions(this.options);
		console.log('kmKlickMeister::updateOptions ' + JSON.stringify(this));
	}

	renderPage() {
		if(typeof(this.renderAccount) === 'undefined') {
			Object.assign(this, new kmRenderer());
		}

        if(this.options.enableAccount) this.renderAccount();
        if(this.options.enableOK) this.renderOK();
        if(this.options.enableOS) this.renderOS();
        if(this.options.enableWork) this.renderWork();
        if(this.options.enableGM) this.renderGM();
        if(this.options.enableMission) this.renderMission();
        if(this.options.enableDuel) this.renderDuel();
        if(this.options.enableTournament) this.renderTournament();
        if(this.options.enableTreasury) this.renderTreasury();

        var closure = this.bind(this.checkOptions);
		var clickers = document.getElementById('km_accountPlugin').getElementsByClassName('kmChecker');
		for(let clicker of clickers) {
			clicker.addEventListener('click', function() {
				closure(this.id);
			});
		}

		//console.log('kmKlickMeister::renderPage ' + JSON.stringify(this));
	}

	checkOptions(what) {
		var test = what.replace('km_enable', '').replace('Actions', '');
		if(typeof(this.state[test]) === 'object') {
			this.state[test].actionsEnabled = document.getElementById(what).checked;
			if(!this.state[test].actionsEnabled && this.state.currentAction === test) {
				delete(this.state.currentAction);
			}
			this.saveState();
		}
	}

	saveState() {
        var request = {action: 'kmSaveState', account: {server: this.server, player: this.player}, state: this.state};
        try {
			chrome.runtime.sendMessage(request);
		} catch(e) {
			window.location.reload();
			return(false);
		}
		return(true);
	}

	hasCooldown() {
		var cTime = parseInt(Date.now() / 1000).toFixed(0);
		if(this.state.Account.cooldown.endTime < cTime) return(false);
		return(this.state.Account.cooldown.mode);
	}

	callReports(force = false) {
		if(this.hasCooldown() && this.state.Reports.last_read > (this.state.Account.cooldown.endTime - this.state.Account.cooldown.duration))
			if(!force) return;
		this.ui.informant.setAttribute('what', 'bk_reports');
        this.ui.responseReady.click();
	}

	readReports(data) {
		var request = {action: 'bkCheckReports', account: {server: this.server, player: this.player}, reports: data.mails};
        chrome.runtime.sendMessage(request, this.bind(this.readReportResponse));
		this.state.Reports.last_read = parseInt(Date.now() / 1000).toFixed(0);
	}

	readReportResponse(response) {
		//console.log('kmKlickMeister::readReportResponse' + JSON.stringify(response));
		if(!response) return(false);
		if(response.result !== 'success') return(false);
		this.state.Duel.reports = [];
		this.state.OS.reports = [];

		for(let report of response.reports) {
			console.log('report: ' + JSON.stringify(report.mail_content));
			switch(report.mail_content.fight_type) {
				case 'duel':
					this.state.Duel.reports.push(report);
					break;
				case 'warfight':
					this.state.OS.reports.push(report);
					break;
				default:
					break;
			}
		}
		//this.state.Duel.reports = response.reports;
        if(this.options.enableOS) this.renderOS();
        if(this.options.enableDuel) this.renderDuel();
		//this.saveState();
	}

	checkActions() {
        //if(!this.options.enableAccount) return;

		var reportFlag = false;
		if(this.state.Account.cooldown.mode === 'duel') {
			if(this.state.Reports.last_read < (this.state.Account.cooldown.endTime - this.state.Account.cooldown.duration))
				reportFlag = true;
		}

        this.pathList = this.splitPath(window.location.pathname);
        switch(this.pathList.shift()) {
            case 'market':
				this.checkMarket();
                break;
			case 'mail':
				if(this.pathList.shift() === 'reports') reportFlag = true;
				break;

			default:
				break;
		}

		if(reportFlag) this.callReports(true);

		this.saveState();

		var msg = {action: 'bkCheckActions', account: {server: this.server, player: this.player}};
		try {
			chrome.runtime.sendMessage(msg,	this.bind(this.actionResponse));
		} catch(e) {
			return(false);
		}

		//console.log('kmKlickMeister::checkActions ' + JSON.stringify(this.state) + ', reportFlag: ' + reportFlag);

		msg.action = 'bkStoreServer';
		try {
			chrome.runtime.sendMessage(msg);
		} catch(e) {
			return(false);
		}
	}

	actionResponse(response) {
		console.log('kmKlickMeister::actionResponse(' + JSON.stringify(response) + ')');
		if(!response) return(false);
		if(response.result !== 'success') return(false);
		var action = response.action;

		switch(action.command) {
			case 'work_work':
				this.actionWorkWork(action);
				break;
			case 'work_encash':
				this.actionWorkEncash();
				break;
			case 'work_finish':
				delete(this.state.currentAction);
				break;
			case 'gm_reload':
				setTimeout(this.bind(this.actionGMReload), action.delay * 1000);
				break;
			default:
				break;
		}

	}

	actionGMReload() {
		console.log('actionGMReload: ' + JSON.stringify(this.state.path));
		if(this.state.path === '/groupmission/group/') window.location.reload();
	}

	// arbeiten schicken
	actionWorkWork(action) {
		var knob = document.getElementById('submit' + action.side);
		if(!knob || knob.classList.contains('disabledBtn')) {
			delete(this.state.currentAction);
			return;
		}

		var submitFormHours = document.getElementById('workFormHours');
		var submitFormSide = document.getElementById('workFormSide');
		var formElement = document.getElementById('workSubmit');

		if(!submitFormHours || !submitFormSide || !formElement) {
			delete(this.state.currentAction);
			return;
		}

		submitFormSide.value = action.side.toLowerCase();
		submitFormHours.value = action.workHours || 1;
		//console.log('actionWorkWork(' + JSON.stringify(action) + ') ' + submitFormHours.value + 'h, ' + submitFormSide.value);
		formElement.submit();
	}

	// Lohn abholen
	actionWorkEncash() {
		var button = document.getElementById('encashLink');
		if(button) button.click();
	}

	checkMarket() {
        switch(this.pathList.shift()) {
			case 'work':
				var options = this.options.Work || {};
				if(!options.actionsEnabled) return;
				break;
			case 'merchant':
				if(this.pathList.shift() === 'artefacts') {
					//this.parseArtefacts();
				}
				break;
			default:
				break;
		}
	}

	buyItem(id) {
        this.ui.informant.setAttribute('what', 'bk_buyitem');
		this.ui.informant.setAttribute('data', id);
        this.ui.responseReady.click();
		console.log('buyItem: ' + id + ' jetzt gekauft!');
	}

	checkTimeout(data) {
		this.ui.removeProgressRow(data);

		switch(data) {
			case 'progressbarTextCooldownProgress':
				this.checkCooldownTimeout();
				break;
			case 'progressbarTextOSProgress':
				this.checkOSTimeout();
				break;
			case 'progressbarTextGMProgress':
				this.checkGMTimeout();
				break;
			case 'progressbarTextTournamentProgress':
				break;
			case 'progressbarTextTreasuryProgress':
				break;
			default:
				console.log('kmKlickMeister::checkTimeout(' + data + ')');
				break;
		}
	}

	checkGMTimeout() {
		var state = this.state.GM || {};
		state.points = 120;
		state.mode = 'search_group';
		this.state.GM = state;
		if(this.options.enableGM) this.renderGM();

		console.log('checkGMTimeout: ' + JSON.stringify(state));
	}

	checkOSTimeout() {
		var state = this.state.OS || {};
		console.log('checkOSTimeout: ' + JSON.stringify(state));

		if(state.round < 9) {
			state.round++;
			state.endTime = parseInt(state.endTime) + parseInt(state.duration);
			this.state.OS = state;
			if(this.options.enableOS) this.renderOS();
		}
	}

	checkCooldownTimeout() {

		if(CONSTANTS.waitingPaths.includes(this.state.path)) {
			return;
		}

		var state = this.state.Account.cooldown || {};
		// {"data":{"missionArt":"small","missionID":"BurningVillage","missionKarma":"Good"},"endTime":"1565164161","duration":"76","mode":"fight"} - /user/
		// {"endTime":"1565164932","duration":"300","mode":"duel"} - /duel/duel/
		console.log('checkCooldownTimeout: ' + JSON.stringify(state) + ' - ' + this.state.path);
        setTimeout(this.bind(this.checkActions), this.action_delay);
	}

}
