class bkOptions {
	constructor({
		Account = {
			enabled: true
		},
		OK = {
			enabled: false
		},
		OS = {
			enabled: false
		},
		Work = {
			enabled: false
		},
		GM = {
			enabled: false
		},
		Mission = {
			enabled: false
		},
		Duel = {
			enabled: false
		},
		Tournament = {
			enabled: false
		},
		Treasury = {
			enabled: false
		}
	} = {}) {
		Object.assign(this, {
			Account,
			OK,
			OS,
			Work,
			GM,
			Mission,
			Duel,
			Tournament,
			Treasury
		});
	}

	bind(method) {
        var _this = this;
        return(function() {
            return(method.apply(_this, arguments));
        });
	}

}
