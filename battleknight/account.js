class kmOptions {
	constructor({
		enableAccount = true,
		enableOK = false,
		enableOS = false,
		enableWork = false,
		enableGM = false,
		enableMission = false,
		enableDuel = false,
		enableTournament = false,
		enableTreasury = false
	} = {}) {
		Object.assign(this, {
			enableAccount,
			enableOK,
			enableOS,
			enableWork,
			enableGM,
			enableMission,
			enableDuel,
			enableTournament,
			enableTreasury
		});
	}
}


class kmAccount {
	constructor({
		server = '',
		player = '',
		options = new kmOptions(),
		state = new bkState()
	} = {}) {
		Object.assign(this, {
			server,
			player,
			options,
			state
		});
	}

	hasCooldown() {
		//console.log(cTime);
		console.log('kmAccount::hasCooldown(' + JSON.stringify(this.state.Account.cooldown) + ')');
		if(!this.state.Account.cooldown.endTime) return(false);

		var cTime = parseInt(Date.now() / 1000).toFixed(0);
		if(this.state.Account.cooldown.endTime < cTime) return(false);
		return(true);
	}

	findAction() {
		var actions = [];
		for(var prop in this.state) {
			//console.log('findAction: (' + prop + ')\n' + typeof(this.state[prop]));
			if(typeof(this.state[prop]) === 'object') {
				if(this.state[prop].actionsEnabled) actions.push(prop);
				//console.log('findAction: (' + prop + ')\n' + JSON.stringify(this.state[prop]));
			}
		}
		console.log('--> findAction: ' + JSON.stringify(actions));
		if(actions.length === 0) return('');
		if(actions.length === 1) return(actions[0]);
		// würfeln
		return(actions[randInt(0, actions.length-1)]);
	}

	checkReports(mails, server) {
		mails.reverse();
		for(let mail of mails) {
			server.parseReport(mail);
		}
		//this.state.Reports.last_read = parseInt(Date.now() / 1000).toFixed(0);
		let count = server.reports.size;
		let it = server.reports.keys();
		var keys = [];

		let result = it.next();
		while(!result.done) {
			keys.push(result.value);
			result = it.next();
		}
		keys.sort();
		keys.reverse();
		var reports = [];
		for(var a = 0; a < keys.length; ++a) {
			var report = server.reports.get(parseInt(keys[a]));
			if(report) reports.push(report.short);
			if(a > 3) break;
		}
		//this.state.Duel.reports = reports;
		//console.log('kmServer::reports: ' + JSON.stringify(reports));
		return(reports);
	}

}
