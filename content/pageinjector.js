window.onerror = null;

class pageInjector {

	constructor() {
        this.km_informant = document.getElementById('km_informant');
        this.km_readyRead = document.getElementById('km_readyRead');
        this.km_readyWrite = document.getElementById('km_readyWrite');
        this.km_responseReady = document.getElementById('km_responseReady');

        this.km_responseReady.addEventListener("click", this.bind(this.listener));

        if(typeof(startTravel) === 'function') {
			// Original-Funktion merken.
			this.km_travel = startTravel;
			// und durch eine eigene ersetzen :-)
			startTravel = this.bind(this.startTravel);
		}

		if(typeof(chooseMission) === 'function') {
			// Original-Funktion merken.
			this.km_mission = chooseMission;
			// und durch eine eigene ersetzen :-)
			chooseMission = this.bind(this.chooseMission);
		}

	}

	bind(method) {
        var _this = this;
        return(function() {
            return(method.apply(_this, arguments));
        });
	}

	listener() {
        var what = km_informant.getAttribute('what');
        var data = km_informant.getAttribute('data');

        switch(what) {
			case 'bk_buyitem':
				this.buyItem(data);
                break;
            case 'bk_vars':
                this.getVars();
                break;
            case 'bk_reports':
				this.getReports();
                break;
            case 'bk_zones':
				this.initZones();
				break;
			case 'bk_inventory':
				this.getInventory(data);
				break;
			case 'bk_lootbox':
				this.getLootbox();
				break;
            default:
                console.log('pageInjector::listener(' + what + '): ' + data + ', ' + window.location.pathname + ', ' + g_recentElement + ', ' + g_recentSubElement);
                //alert('pageInjector::listener(' + what + '): ' + data + ', ' + window.location.pathname + ', ' + g_recentElement + ', ' + g_recentSubElement);
                break;
        }
        //console.log('pageInjector::listener() ' + what);
	}

	buyItem(id) {
		var item = $(id);
		var tmp = item.retrieve('item:item_id');
		if(!tmp) tmp = item.retrieve('item:id');

        var fields = {
            x: 0,
            y: 0
        };
        fields = g_dragFunctions.determinePosition({x: 0, y: 300});
        //for (var x = 0; x < g_inventorySize.width; x++) {
            //for (var y = 0; y < g_inventorySize.depth; y++) {
                //if (pos.x >= ((x - .5) * g_inventoryFieldWidth) && pos.x < ((x + .5) * g_inventoryFieldWidth) && pos.y >= ((y - .5) * g_inventoryFieldDepth) && pos.y < ((y + .5) * g_inventoryFieldDepth)) {
        //item.retrieve('item:allAttributes')
		console.log('pageInjector::buyItem(' + tmp + ') ' + ' :: ' + JSON.stringify(fields));
		//g_dragFunctions.buyItem(tmp, g_activeTabID, 0, 5);
	}

	// how:
	// foot, horse, caravan, long_boat, cog
	startTravel(where, how, el, premium) {
		if(typeof(this.km_travel) !== 'function') return;
		if(el.className.test('disabledBtn')) return;

		// Infos übergeben
        this.km_informant.setAttribute('what', 'bk_travel');
        this.km_informant.setAttribute('data', JSON.stringify(arguments));
        this.km_readyWrite.click();

		// Original-Funktion aufrufen.
		this.km_travel(where, how, el, premium);
	}

	chooseMission(missionArt, missionID, missionKarma, el, buyRubies) {
		if(typeof(this.km_mission) !== 'function') return;
		if(el.className.test('disabledSpecialBtn')) return;

		// Infos übergeben
        this.km_informant.setAttribute('what', 'bk_mission');
        this.km_informant.setAttribute('data', JSON.stringify(arguments));
        this.km_readyWrite.click();

		// Original-Funktion aufrufen.
		this.km_mission(missionArt, missionID, missionKarma, el, buyRubies);
	}

	initZones() {
		$$('.targetZone1').store('tip:title', 'Rechte Schulter');
		$$('.targetZone1').store('tip:text', '- Trefferchance <b>-25%</b><br/>- Schaden <b>+25%</b>');
		g_toolTips.attach($$('.targetZone1'));
		$$('.targetZone2').store('tip:title', 'Kopf');
		$$('.targetZone2').store('tip:text', '- Trefferchance <b>-10%</b><br/>- Schaden <b>+10%</b>');
		g_toolTips.attach($$('.targetZone2'));
		$$('.targetZone3').store('tip:title', 'Linke Schulter');
		$$('.targetZone3').store('tip:text', '- Trefferchance <b>+/-0%</b><br/>- Schaden <b>+/-0%</b>');
		g_toolTips.attach($$('.targetZone3'));
		$$('.targetZone4').store('tip:title', 'Rechter Arm');
		$$('.targetZone4').store('tip:text', '- Trefferchance <b>-10%</b><br/>- Schaden <b>+10%</b>');
		g_toolTips.attach($$('.targetZone4'));
		$$('.targetZone5').store('tip:title', 'Brust');
		$$('.targetZone5').store('tip:text', '- Trefferchance <b>+25%</b><br/>- Schaden <b>-25%</b>');
		g_toolTips.attach($$('.targetZone5'));
		$$('.targetZone6').store('tip:title', 'Linker Arm');
		$$('.targetZone6').store('tip:text', '- Trefferchance <b>+10%</b><br/>- Schaden <b>-10%</b>');
		g_toolTips.attach($$('.targetZone6'));
		$$('.targetZone7').store('tip:title', 'Rechtes Bein');
		$$('.targetZone7').store('tip:text', '- Trefferchance <b>-10%</b><br/>- Schaden <b>+10%</b>');
		g_toolTips.attach($$('.targetZone7'));
		$$('.targetZone8').store('tip:title', 'Zentrum');
		$$('.targetZone8').store('tip:text', '- Trefferchance <b>+10%</b><br/>- Schaden <b>-10%</b>');
		g_toolTips.attach($$('.targetZone8'));
		$$('.targetZone9').store('tip:title', 'Linkes Bein');
		$$('.targetZone9').store('tip:text', '- Trefferchance <b>+/-0%</b><br/>- Schaden <b>+/-0%</b>');
		g_toolTips.attach($$('.targetZone9'));
		$$('.defendZone1').store('tip:title', 'Rechter Oberk&ouml;rper');
		$$('.defendZone1').store('tip:text', 'Geblockte Zonen:<br/>- Rechte Schulter<br/>- Rechter Arm');
		g_toolTips.attach($$('.defendZone1'));
		$$('.defendZone2').store('tip:title', 'Linker Oberk&ouml;rper');
		$$('.defendZone2').store('tip:text', 'Geblockte Zonen:<br/>- Linke Schulter<br/>- Linker Arm');
		g_toolTips.attach($$('.defendZone2'));
		$$('.defendZone3').store('tip:title', 'Zentrum');
		$$('.defendZone3').store('tip:text', 'Geblockte Zonen:<br/>- Kopf<br/>- Brust');
		g_toolTips.attach($$('.defendZone3'));
		$$('.defendZone4').store('tip:title', 'Rechte Seite');
		$$('.defendZone4').store('tip:text', 'Geblockte Zonen:<br/>- Rechter Arm<br/>- Rechtes Bein<br/>- Brust<br/>- Zentrum');
		g_toolTips.attach($$('.defendZone4'));
		$$('.defendZone5').store('tip:title', 'Linke Seite');
		$$('.defendZone5').store('tip:text', 'Geblockte Zonen:<br/>- Linker Arm<br/>- Linkes Bein<br/>- Brust<br/>- Zentrum');
		g_toolTips.attach($$('.defendZone5'));
	}

	getPlayerVars() {
        var data = {};

        var txt = document.id('silver').retrieve('tip:text');
        data.treasury = txt.split(' ').pop();
        data.silver = g_silver;
        data.rubies = g_rubies;
        data.health = g_health;
        data.maxHealth = g_maxHealth;
        data.abilities = g_userSpecialAbilities;

        if(typeof(knightLevel) !== 'undefined') data.level = knightLevel;
        if(typeof(karmaPts) !== 'undefined') data.karmaPts = karmaPts;

        // inventory
        if(typeof(g_activeTabID) !== 'undefined') data.activeTab = g_activeTabID;
        if(typeof(g_inventorySize) !== 'undefined') data.inventorySize = g_inventorySize;
        if(typeof(g_inventoryString) !== 'undefined') data.inventoryString = g_inventoryString;
        if(typeof(g_accessibleInventories) !== 'undefined') data.accessibleInventories = g_accessibleInventories;

        return(data);
	}

	getServerVars() {
        var data = {};

        if(typeof(g_speedFactor) !== 'undefined') data.speedFactor = g_speedFactor;

		// timers
        if(typeof(l_titleTimerEndTime) !== 'undefined') data.titleTimer = l_titleTimerEndTime;

        if(typeof(progressbarEndTime) !== 'undefined')
			data.progressbarEndTime = parseInt(progressbarEndTime + Date.now() / 1000).toFixed(0);
        if(typeof(progressbarDuration) !== 'undefined')
			data.progressbarDuration = progressbarDuration;

        // working
        if(typeof(availableSides) !== 'undefined') data.availableSides = availableSides;
        if(typeof(workConfig) !== 'undefined') data.workConfig = workConfig;
        if(typeof(availableWorkingHours) !== 'undefined') data.availableWorkingHours = availableWorkingHours;

        // travel
        if(typeof(mapBasePosition) !== 'undefined') data.mapBasePosition = mapBasePosition;
        if(typeof(highlightedRoutes) !== 'undefined') data.highlightedRoutes = highlightedRoutes;

		// groupmission
        if(typeof(inGroup) !== 'undefined') data.inGroup = inGroup;
        if(typeof(grpData) !== 'undefined') data.grpData = grpData;

        return(data);
	}

	getMerchantVars() {
        var data = {};

		var things = $$('#merchItemLayer .merchantItem');
		things.forEach(function(current, index, listObj) {
			if(current.classList.contains('itemClue01_closed')) {
				data.clue = current.retrieve('item:allAttributes');
			}
		}, this);

        return(data);
	}

	getVars() {
        var data = {};
        data.player = this.getPlayerVars();
        data.server = this.getServerVars();
        data.merchant = this.getMerchantVars();

        this.km_informant.setAttribute('data', JSON.stringify(data));
        this.km_readyWrite.click();
        //console.log('getVars: ' + JSON.stringify(data));
	}

	getLootbox() {
		var lootItems = $$('#lootContent .lootItem');
		var ret = [];
		lootItems.each(function(elem, index) {
			//console.log('lootItem: ' + Object.keys(elem.retrieve('item:allAttributes')));
			ret.push(elem.retrieve('item:allAttributes'));
		});
        this.km_informant.setAttribute('data', JSON.stringify(ret));
        this.km_readyWrite.click();
	}

	getInventory(tabID = 1) {
        var oneRequest = new Request.JSON({
            url: g_base_request + 'getInventory/',
            noCache: true,
			method: "get",
			data: {
				'inventory': tabID,
				'loc': g_DNDLocation
			},
            onRequest: function() {}
            .bind(this),
            onSuccess: function(data) {
                this.km_informant.setAttribute('what', 'bk_inventory');
                this.km_informant.setAttribute('data', JSON.stringify(data));
                this.km_readyWrite.click();
                return(true);
            }
            .bind(this),
            onFailure: function() {
                return(false);
            }
            .bind(this)
		}).send();
	}

	getReports(page = 1, search = '') {
        var oneRequest = new Request.JSON({
            url: g_url + '/mail/getInbox/',
            noCache: true,
            data: {
                'inboxtype': 'reports',
                'page': page,
                'searchstring': search,
                'recentElement': 'Messages',
                'recentSubElement': 'Reports'
            },
            onRequest: function() {}
            .bind(this),
            onSuccess: function(data) {
                this.km_informant.setAttribute('what', 'bk_reports');
                this.km_informant.setAttribute('data', JSON.stringify(data));
                this.km_readyWrite.click();
            }
            .bind(this),
            onFailure: function() {
                return(false);
            }
            .bind(this)
        }).send();
	}

}

new pageInjector();
