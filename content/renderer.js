function kmRenderer() {

	this.renderAccount = function() {
		if(!this.state) return;
		var state = this.state.Account;
		var now = parseInt(Date.now() / 1000);
		if(state.karma) {
			if(state.karma.modus && parseInt(state.karma.endTime) > now) {
				var text = userKarma[state.karma.modus].name;
				switch(parseInt(state.karma.modus)) {
					case 7:
					case 10:
						text = text.split(' ').shift();
						break;
					case 3:
					case 8:
						text = text.split(' ').pop();
						break;
					default:
						break;
				}
				var bar = this.ui.addProgressBar('bkAccountModule', 'KarmaProgress', text, state.karma.endTime, state.karma.duration);
				var progress = bar.querySelector('div.km_progressbar');
				progress.setAttribute("km_direction", "down");
				progress.setAttribute("title", userKarma[state.karma.modus].title);
				//progress.store('tip:title', userKarma[state.modus].title);
				progress.classList.add("mediumToolTip");
				progress = bar.querySelector('div.kmProgressMeter');
				progress.style.backgroundColor = 'rgba(0,255,0, 0.35)';
			}
		}

		if(!state.cooldown) return;

		if(state.cooldown.mode && parseInt(state.cooldown.endTime) > now) {
			var text = state.cooldown.mode;
			var bar = this.ui.addProgressBar('bkAccountModule', 'CooldownProgress', text, state.cooldown.endTime, state.cooldown.duration);
		}
	};

    this.renderOK = function() {
		var state = this.state.OK;

		if(!state.castle) {
			var ok_link = document.getElementById('bkOKLink');
			ok_link.href='/clan';
		}

		var bar = document.getElementById('bkOKModule');
		if(!bar) bar = this.ui.addProgressBar('bkOKModule','OKProgress', '', state.current, state.payment);

		if(state.current >= state.payment) {
			bar = bar.querySelector('thead tr');
			bar.style.backgroundColor = 'rgba(32,237,12,0.35)';
		}
		bar = bar.querySelector('div.km_progressbar');
		if(bar) bar.setAttribute("km_timer", "off");
	};

    this.renderOS = function() {
		var state = this.state.OS || {};
		var clicker = document.getElementById('km_enableOSActions');
		if(clicker) clicker.checked = state.actionsEnabled;

		var line = document.getElementById('OSProgressText');
        var cTime = parseInt(Date.now() / 1000).toFixed(0);
        var text = '';

		// unknown, peace, prepare, battle
		switch(state.mode) {

			case undefined:
				if(!line) {
					line = this.ui.appendLine('bkOSModule');
					line.setAttribute('id', 'OSProgressText');
				}
				line.innerHTML = 'unbekannt';
				break;

			case 'peace':
				var bar = document.getElementById('bkOSModule');
				bar = bar.querySelector('thead tr');
				bar.style.backgroundColor = 'rgba(32,237,12,0.35)';
				break;

			case 'prepare':
				break;

			case 'battle':
				break;

			default:
				break;

		}

		if(state.mercenary) {
			var link = document.getElementById('bkOSModule');
			link = link.querySelector('a');
			link.href = '/tavern/battle';
		}
        if(!state.endTime || cTime >= state.endTime) return;

		if(!state.duration) state.duration = (8 * 60 * 60);
        if(state.round) text = 'Runde ' + state.round;

        var bar = this.ui.addProgressBar('bkOSModule','OSProgress', text, state.endTime, state.duration);

		var row = 0;
		for(let report of state.reports) {
			var line = document.getElementById('bkOSOpponent'+row);
			if(!line) {
				line = this.ui.appendLine('bkOSModule');
				line.setAttribute('id', 'bkOSOpponent'+row);
				line.setAttribute('align', 'left');
			}
			this.renderDuelReport(line, report);
			row++;
		}
    };

	this.renderWork = function() {
		var state = this.state.Work;
		var clicker = document.getElementById('km_enableWorkActions');
		if(clicker) clicker.checked = state.actionsEnabled;
	};

    this.renderGM = function() {
		var state = this.state.GM;
		var clicker = document.getElementById('km_enableGMActions');
		if(clicker) clicker.checked = state.actionsEnabled;

		var line = document.getElementById('GMProgressText');
		switch(state.mode) {

			case 'unknown':
				if(!line) {
					line = this.ui.appendLine('bkGMModule');
					line.setAttribute('id', 'GMProgressText');
				}
				line.innerHTML = 'unbekannt';
				break;

			case 'in_group':
				var bar = document.getElementById('bkGMModule');
				bar = bar.querySelector('thead tr');
				bar.style.backgroundColor = 'rgba(32,237,12,0.35)';
				break;

			case 'search_group':
				if(!line) {
					line = this.ui.appendLine('bkGMModule');
					line.setAttribute('id', 'GMProgressText');
				}
				line.innerHTML = 'suche Gruppe';
				break;

			default:
				var text = 'voll in';
				var end = parseInt(state.endTime) || 0;
				var dur = parseInt(state.duration) || 0;
				var bar = this.ui.addProgressBar('bkGMModule', 'GMProgress', text, end, dur);
				var meter = bar.querySelector('div.kmProgressMeter');
				meter.style.backgroundColor = 'rgba(255,255,0, 0.35)';
				var now = parseInt(Date.now() / 1000);
				if(end < now) {
					bar = bar.querySelector('div.km_progressbar');
					bar.setAttribute("km_timer", "off");
				}
				break;

		}
	};

    this.renderMission = function() {
		var state = this.state.Mission || {};
		var clicker = document.getElementById('km_enableMissionActions');
		if(clicker) clicker.checked = state.actionsEnabled;

		var line = document.getElementById('MissionProgressText');
		if(!line) {
			line = this.ui.appendLine('bkMissionModule');
			line.setAttribute('id', 'MissionProgressText');
		}
		var text = 'unbekannt';
		if(state.location) text = locations[state.location].title;
		var points = state.points || '0';
		//if(state.points) points = state.points;
		line.innerHTML = `<div class="km_progressText" style="display: inline; padding-right: 5px;">${text}</div><span id="bkMissionPoints">${points}</span>`;
	};

	this.renderDuel = function() {
		var state = this.state.Duel || {};
		state.reports = state.reports || [];
		var clicker = document.getElementById('km_enableDuelActions');
		if(clicker) clicker.checked = state.actionsEnabled;

		var row = 0;
		for(let report of state.reports) {
			var line = document.getElementById('bkDuelOpponent'+row);
			if(!line) {
				line = this.ui.appendLine('bkDuelModule');
				line.setAttribute('id', 'bkDuelOpponent'+row);
				line.setAttribute('align', 'left');
			}
			this.renderDuelReport(line, report);
			row++;
		}
	};

	this.renderDuelReport = function(line, report) {
		var fighter;
		var winner = false;
		if(parseInt(this.player) === parseInt(report.mail_content.aggressor.id)) {
			fighter = report.mail_content.defender;
			if(report.mail_content.aggressor.fightResult === 'won') winner = true;
		}
		if(parseInt(this.player) === parseInt(report.mail_content.defender.id)) {
			fighter = report.mail_content.aggressor;
			if(report.mail_content.defender.fightResult === 'won') winner = true;
		}

		//console.log('KlickMeister::renderDuelReport ' + this.player + '::' + report.mail_content.aggressor.id + ', ' + JSON.stringify(report.mail_content));

		var send = new Date(report.mail_senttime*1000);
		var hours = send.getHours() || 0;
		if(hours < 10)   hours = '0' + hours.toString();
		var minutes = send.getMinutes() || 0;
		if(minutes < 10) minutes = '0' + minutes.toString();

		var link = '/common/profile/' + fighter.id + '/Messages/Reports';

		if(fighter.name.length > 23) {
			fighter.name = fighter.name.substring(0, 20) + '...';
		}
		var profile = `<a href="${link}" style="margin-right: 5px; white-space: nowrap;">${fighter.name}</a>`;
		line.innerHTML = `<div style="display: inline;">${hours}:${minutes}</div><div style="display: inline; padding-left: 3px; width: 100%;">${profile}</div>`;

		if(winner) line.style.backgroundColor = 'rgba(32,237,12,0.35)';
        else line.style.backgroundColor = 'rgba(255,0,0, 0.35)';
	}; // renderDuelReport

    this.renderTournament = function() {
		var state = this.state.Tournament;
		var clicker = document.getElementById('km_enableTournamentActions');
		if(clicker) clicker.checked = state.actionsEnabled;

		var line = document.getElementById('TournamentProgressText');
        if(state.round < 0) {
            if(!line) {
				line = this.ui.appendLine('bkTournamentModule');
				line.setAttribute('id', 'TournamentProgressText');
			}
            line.innerHTML = 'nicht angemeldet';
            return;
        }
        if(state.round === 0) {
            if(!line) {
				line = this.ui.appendLine('bkTournamentModule');
				line.setAttribute('id', 'TournamentProgressText');
			}
            line.innerHTML = 'angemeldet';
            return;
        }
        if(!state.endTime) return;
        var cTime = parseInt(Date.now() / 1000).toFixed(0);
        if(cTime >= state.endTime) return;
        var text = '';
        if(state.round) text = 'Runde ' + state.round + ' in';
        var bar = this.ui.addProgressBar('bkTournamentModule', 'TournamentProgress', text, state.endTime, state.duration);
    };

    this.renderTreasury = function() {
		var state = this.state.Treasury;
		var clicker = document.getElementById('km_enableTreasuryActions');
		if(clicker) clicker.checked = state.actionsEnabled;

        if(!state.endTime) return;
        var cTime = parseInt(Date.now() / 1000).toFixed(0);
        if(cTime >= state.endTime) return;
        var bar = this.ui.addProgressBar('bkTreasuryModule', 'TreasuryProgress', '', state.endTime, 6 * 60 * 60);
        bar = bar.querySelector('div.kmProgressMeter');
        bar.style.backgroundColor = 'rgba(255,255,0, 0.35)';
    };

}
