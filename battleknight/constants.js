var locations = {
	byName: function(name) {
		locations.forEach(function(current, index) {
			if(current.name === name) {
				return(current);
			}
		}, this);
		return({});
	},
	byTitle: function(title) {
		locations.forEach(function(current, index) {
			if(current.title === title) {
				return(current);
			}
		}, this);
		return({});
	},
    "v1": {
        "name": "VillageOne",
        "title": "Tarant",
        "tax": 41,
        "missions": [
            "BanditLair",
            "Cave",
            "StoneCircle",
            "Coast"
        ],
        "work": {
            "evil": 0.6,
            "good": 0.6,
            "neutral": 0.66
        }
    },
    "tp1": {
        "name": "TradingPostOne",
        "title": "Grand",
        "tax": 36,
        "missions": [
            "OldOak",
            "StoneCircle"
        ],
        "work": {
            "neutral": 0.55
        }
    },
    "h3": {
        "name": "HarbourThree",
        "title": "Sedwich",
        "tax": 41,
        "missions": [
            "GroundedShip",
            "Harbour",
            "Lighthouse"
        ],
        "work": {
            "evil": 0.8,
            "good": 0.8,
            "neutral": 0.88
        }
    },
    "hs": {
        "name": "CapitalCity",
        "title": "Endalain",
        "tax": 35,
        "missions": [
            "VictoryColumn",
            "MysticalTower"
        ],
        "work": {
            "evil": 1.0,
            "good": 1.0,
            "neutral": 1.1
        }
    },
    "cf1": {
        "name": "CoastalFortressOne",
        "title": "Asgal",
        "tax": 44,
        "missions": [
            "BurningVillage",
            "AdversaryBridgehead",
            "Coast"
        ],
        "work": {
            "evil": 1.1,
            "good": 1.1,
            "neutral": 1.21
        }
    },
    "cf2": {
        "name": "CoastalFortressTwo",
        "title": "Gastain",
        "tax": 45,
        "missions": [
            "Watchtower",
            "BurningVillage",
            "AdversaryBridgehead",
            "GroundedShip"
        ],
        "work": {
            "evil": 1.1,
            "good": 1.1,
            "neutral": 1.21
        }
    },
    "c1": {
        "name": "CityOne",
        "title": "Alcran",
        "tax": 40,
        "missions": [
            "Tabour",
            "Bridge",
            "Coast"
        ],
        "work": {
            "evil": 0.8,
            "good": 0.8,
            "neutral": 0.88
        }
    },
    "v2": {
        "name": "VillageTwo",
        "title": "Hatwig",
        "tax": 37,
        "missions": [
            "Bridge",
            "Swamp",
            "Coast",
            "Forest"
        ],
        "work": {
            "evil": 0.6,
            "good": 0.6,
            "neutral": 0.66
        }

    },
    "tp2": {
        "name": "TradingPostTwo",
        "title": "Talmet",
        "tax": 34,
        "missions": [
            "Cave",
            "Forest"
        ],
        "work": {
            "neutral": 1
        }
    },
    "h1": {
        "name": "HarbourOne",
        "title": "Waile",
        "tax": 36,
        "missions": [
            "GroundedShip",
            "Harbour",
            "Lighthouse"
        ],
        "work": {
            "evil": 0.9,
            "good": 0.9,
            "neutral": 0.99
		}
    },
    "h2": {
        "name": "HarbourTwo",
        "title": "Alvan",
        "tax": 39,
        "missions": [
            "Bay",
            "Harbour",
            "Cliffs"
        ]
    },
    "tp4": {
        "name": "TradingPostFour",
        "title": "Milley",
        "tax": 33,
        "missions": [
            "OldOak",
            "Mountain"
        ]
    },
    "v4": {
        "name": "VillageFour",
        "title": "Jarow",
        "tax": 42,
        "missions": [
            "Tabour",
            "CastleRuin",
            "Swamp",
            "Cliffs"
        ]
    },
    "f2": {
        "name": "FortressTwo",
        "title": "Segur",
        "tax": 48,
        "missions": [
            "Watchtower",
            "BurningVillage",
            "AdversaryBridgehead"
        ]
    },
    "tp3": {
        "name": "TradingPostThree",
        "title": "Brant",
        "tax": 38,
        "missions": [
            "Mountain",
            "Volcano"
        ],
        "work": {
            "neutral": 0.55
        }
    },
    "v3": {
        "name": "VillageThree",
        "title": "Ramstill",
        "tax": 43,
        "missions": [
            "ArkanianHills",
            "Mountain",
            "Cave",
            "Coast"
        ],
        "work": {
            "evil": 0.6,
            "good": 0.6,
            "neutral": 0.66
        }
    },
    "f1": {
        "name": "FortressOne",
        "title": "Thulgar",
        "tax": 46,
        "missions": [
            "BurningVillage",
            "AdversaryBridgehead",
            "Cliffs",
            "Forest"
        ],
        "work": {
            "evil": 1.1,
            "good": 1.1,
            "neutral": 1.21
        }
    },
    "gt": {
        "name": "GhostTown",
        "title": "Talfour",
        "tax": 40,
        "missions": [
            "WitchForest",
            "DeadLand",
            "Volcano",
            "CastleRuin",
            "DragonLair"
        ]
    },
    "fi": {
        "name": "FogIsland",
        "title": "Fehan",
        "tax": 35,
        "missions": [
            "Laguna",
            "Tidesbeach",
            "Fogforest"
        ],
        "work": {
            "evil": 1.0,
            "good": 1.0,
            "neutral": 1.1
        }
    }
}

var targetAreas = {
    "Attack": {
        "1": ["r-S", "rechte Schulter"],
        "2": ["-K-", "Kopf"],
        "3": ["l-S", "linke Schulter"],
        "4": ["r-A", "rechter Arm"],
        "5": ["-B-", "Brust"],
        "6": ["l-A", "linker Arm"],
        "7": ["r-B", "rechtes Bein"],
        "8": ["-Z-", "Zentrum"],
        "9": ["l-B", "linkes Bein"]
    },
    "Defend": {
        "1": ["r-O", "rechter Oberkörper"],
        "2": ["l-O", "linker Oberkörper"],
        "3": ["-Z-", "Zentrum"],
        "4": ["r-S", "rechte Seite"],
        "5": ["l-S", "linke Seite"]
    }
}

var userKarma = {
    "1": {
        "name": "Dunkle Kraft",
        "title": "Die dunkle Kraft verleiht dir die Fähigkeit, 5 Stärkepunkte von deinem Gegner für die Dauer des Kampfes auf dich zu übertragen.",
        "cost": 50
    },
    "2": {
        "name": "Heilige Stärke",
        "title": "Dein unerschütterlicher Glaube weckt übermenschliche Stärke in dir. Du erhältst für die Wirkungsdauer 10 zusätzliche Stärkepunkte.",
        "cost": 50
    },
    "3": {
        "name": "Schrei der Todesfee",
        "title": "Die dunkle Kraft aus dem Reich der Toten umgibt deine Waffe mit einer mystischen Macht. Du erhältst 15% des verursachten Gesamtschadens als Lebensenergie gutgeschrieben.",
        "cost": 150
    },
    "4": {
        "name": "Verdammnis",
        "title": "Du überträgst Krankheit und Pestilenz auf deine Gegner, um ihre Widerstandsfähigkeit zu brechen. Alle Resistenzen des Gegners werden um 10% reduziert.",
        "cost": 50
    },
    "5": {
        "name": "Säureschlag",
        "title": "Der Säureschlag umgibt deine Waffenhand mit einer pulsierenden, magischen Säureschicht. Er verursacht einen Schaden von 200% des Grundschadens der getragenen Waffe und trifft immer. Der Angriff wird einmalig zu Beginn eines Kampfes durchgeführt.",
        "cost": 150
    },
    "6": {
        "name": "Dornenpanzer",
        "title": "Der Dornenpanzer umhüllt deine Rüstung mit vergifteten Stacheln und Dornen und verursacht so bei deinem Angreifer Schaden von 15% seines eigenen Gesamtschadens.",
        "cost": 200
    },
    "7": {
        "name": "Schatten der Finsternis",
        "title": "Du beschwörst dunklen Nebel aus dem Unterreich herbei und umgibst dich mit ihm. Der Nebel macht es für einen Angreifer besonders schwer, dich zu treffen. Du erhältst bei jedem Schlag, der dich treffen würde, eine 10% Chance, dem Schlag zu entgehen.",
        "cost": 75
    },
    "8": {
        "name": "Mystischer Schild",
        "title": "Der mystische Schild umgibt deine Rüstung mit einer zusätzlichen Panzerung, die Schäden abwehrt. Der Schutzpanzer absorbiert in jedem Kampf physische Schadenspunkte in Höhe deiner Konstitutionspunkte.",
        "cost": 200
    },
    "9": {
        "name": "Engelsflügel",
        "title": "Durch deinen standhaften Glauben an die Götter wirst du mit den Flügeln eines Engels belohnt, welche dir zusätzlich 30 Missionspunkte ermöglichen.",
        "cost": 150
    },
    "10": {
        "name": "Schutz der Ahnen",
        "title": "Die Helden vergangener Zeiten durchfluten dich mit einem Teil ihrer Macht und erhöhen deine Resistenzen um 10%.",
        "cost": 50
    },
    "11": {
        "name": "Regeneration",
        "title": "Heilende Kräfte durchfluten dich und ermöglichen dir so eine schnellere Regeneration deines Körpers um 50%.",
        "cost": 75
    },
    "12": {
        "name": "Trutzburg",
        "title": "Du wirst durch die mystischen Kräfte einer Gottheit beschützt und kannst von keinem anderen Ritter angegriffen werden.",
        "cost": 225
    }
}

var CONSTANTS = {
	waitingPaths:  [
		"/world/location",
		"/world/travel",
		"/groupmission/group/",
		"/market/work",
		"/duel/",
		"/defiance/"
	]
}

function randInt(low, high) {
    return(Math.floor(Math.random() * (high - low + 1)) + low);
}
