/*
 * BattleKnight Account (content)
 */
function bkAccount() {
	Object.assign(this, new KlickMeister());
    this.server = window.location.hostname.split('.')[0];
    this.player = 0;

	this.initialize = function() {
        var pDiv = document.getElementById('shieldNeutral');
        if(!pDiv) {
            //alert('nicht im Spiel! ＼(＾O＾)／');
            return;
        }
        this.player = parseInt(this.splitPath(pDiv.href)[4]);
	};

	this.initialize();
}

function KlickMeister() {
	// utils / tools
    this.bind = function(method) {
        var _this = this;
        return(function() {
            return(method.apply(_this, arguments));
        });
    };

    this.splitPath = function(path) {
        var paths = [];
        var splitter = path.split('/');
        splitter.forEach(function(item) {
            if(item !== '') paths.push(item);
        });
        return(paths);
    };

    this.splitSearch = function(line, key) {
		if(!line) return;
        var gebastel = line.substr(1);
        gebastel = gebastel.split('&');
        for(var i = 0; i < gebastel.length; ++i) {
            var items = gebastel[i].split('=');
            if(items[0] === key) {
                return(items[1]);
            }
        }
    };
}
