chrome.runtime.onInstalled.addListener(function(details) {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [
                new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {urlMatches: 's[0-9]{1,2}-de.battleknight.gameforge.com', schemes: ['https']}
                })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

console.time("kmBackgroundManager");
var manager = new kmManager();
console.timeEnd("kmBackgroundManager");
