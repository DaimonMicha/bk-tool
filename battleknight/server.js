class kmServer extends kmObject {

	constructor({
		world = ''
	} = {}) {
		super();
		Object.assign(this, {
			world
		});
		this.knights = new Map();
		this.clans = new Map();
		this.reports = new Map();
	}

	get name() {
		return(this.world);
	}

	setKnight(knight) {
		if(knight.type && knight.type !== 'human') return;
		var fighter = this.getKnight(knight.id);
		fighter.merge(knight);
		this.knights.set(fighter.id, fighter);
		//console.log('kmServer::setKnight ' + JSON.stringify(fighter));
	}

	getKnight(kid) {
		if(this.knights.has(kid)) {
			return(this.knights.get(kid));
		}
		var knight = new kmKnight({id: kid});
		this.knights.set(kid, knight);
		//console.log('kmServer::getKnight ' + JSON.stringify(knight));
		return(knight);
	}

	parseReport(report) {
		var mail_id = parseInt(report.mail_id);
		if(this.reports.has(mail_id)) return;
		if(parseInt(report.mail_type) !== 0 && parseInt(report.mail_type) !== 3) return;
		var mail = new bkReport(report);
		if(mail.is_fightReport) {
			this.setKnight(mail.mail_content.aggressor);
			this.setKnight(mail.mail_content.defender);
			this.reports.set(mail_id, mail);
			//console.log('kmServer::parseReport(' + this.world + ') ' + JSON.stringify(mail.mail_content.defender));
		}
	}

}
