class bkAction {
	constructor(account, server) {
		Object.assign(this, {account, server});
	}

	check() {
		//console.log('bkAction::check()');
	}

	run() {
		console.log('bkAction::run()');
	}
}




class osAction extends bkAction {
	/*
	 * states:
	 *
	 * working
	 * go_work
	 * encash
	 * finish
	 */
	constructor(account, server) {
		super(account, server);
	}

	check(cooldown) {
		super.check();
		var state = this.account.state.OS;
		var action = {mode: state.mode};

		console.log('check: ' + JSON.stringify(state));
	}
}



class workAction extends bkAction {
	/*
	 * states:
	 *
	 * go_work_site
	 * working
	 * go_work
	 * encash
	 * finish
	 * blocked (groupmission)
	 */
	constructor(account, server) {
		super(account, server);
	}

	getSide() {
		var knight = this.server.getKnight(this.account.player);
		var srv = this.account.state.Server;
		if(!srv.availableSides) return('');
		var side = 'Neutral';
		if(knight.karma > 0 && srv.availableSides.includes('good')) side = 'Good';
		if(knight.karma < 0 && srv.availableSides.includes('evil')) side = 'Evil';

		return(side);
	}

	check(cooldown) {
		super.check();
		var mode = this.account.state.Work.mode;
		var action = {mode: mode};
		if(!cooldown && mode === 'finish') mode = 'go_work';

		var acc = {
			server: this.account.server,
			player: this.account.player
		};

		switch(mode) {
			case 'go_work':
				action.command = 'work_work';
				var srv = this.account.state.Server;
				var wH = [1,3,1];//5,7,9,11]

/*
				if(srv.availableWorkingHours === 12) {
					wH.push(9);
					wH.push(1);
					wH.push(11);
				}
*/

				//action.workHours = wH[randInt(1, wH.length) - 1];
				action.workHours = 1;
				action.side = this.getSide();
				//console.log('--> go_work for: ' + JSON.stringify(srv) + ' :: ' + wH);
				break;
			case 'encash':
				action = {command: 'work_encash'};
				break;
			default:
				break;
		}

		if(!this.account.state.Work.actionsEnabled) action = {command: 'work_finish'};
		console.log('workAction::check(' + JSON.stringify(acc) + ') ' + JSON.stringify(this.account.state.Work) + ' cooldown(' + cooldown + ') ' + this.account.state.path);
		return(action);
	}
}



class duelAction extends bkAction {
	/*
	 * states:
	 *
	 * working
	 * go_work
	 * encash
	 * finish
	 */
	constructor(account, server) {
		super(account, server);
	}

	check(cooldown) {
		super.check();
	}
}



class gmAction extends bkAction {
	/*
	 * states:
	 *
	 * working
	 * in_group
	 * encash
	 * finish
	 */
	constructor(account, server) {
		super(account, server);
	}

	check(cooldown) {
		super.check();
		var state = this.account.state.GM;
		var action = {mode: state.mode};

		var acc = {
			server: this.account.server,
			player: this.account.player
		};

		switch(state.mode) {
			case 'in_group':
				action.command = 'gm_reload';
				action.delay = randInt(376, 20*60);
				break;
			case 'search_group':
				if(state.groups > 0) {
					action.command = 'gm_join';
					action.delay = randInt(428, 2637);
				}
			default:
				break;
		}

		console.log('gmAction::check ' + JSON.stringify(state));
		return(action);
	}
}
